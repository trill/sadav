# sa::dav

CalDAV/CardDAV client library and command line CalDAV/CardDAV client.

## Dependencies

* https://curl.se/libcurl/
* https://github.com/HowardHinnant/date
* https://github.com/zeux/pugixml
* [libnotify](https://gitlab.gnome.org/GNOME/libnotify) (optional)

## Build

Clone, build and install [date.h](https://github.com/HowardHinnant/date) with `BUILD_TZ_LIB`
or install from [AUR](https://aur.archlinux.org/packages/date-git).

~~~sh
git clone https://codeberg.org/trill/sadav.git
cd sadav
mkdir build
cd build
cmake ..
make
~~~

## Usage

~~~cpp
sa::dav::Calendar client("http://server.com/path/to/calendar/",
    "username", "password");

// Current month
sa::dav::DateTime start = sa::dav::DateTime::Now();
// Go to the first month day
start.SetDate(start.Year(), start.Month(), 1);
start.SetTime(0, 0, 0);
sa::dav::DateTime end = start;
// Add 1 month
end.Add(0, 1, 0);

auto events = client.GetEvents(start, end, sa::dav::Event::Type::Event);
for (const auto& event : events)
{
    std::cout << event.name_ << ": "
        << event.startDate_.Format("%d.%m.%Y") << " "
        << event.startDate_.Format("%H:%M") << "-"
        << event.endDate_.Format("%H:%M") << std::endl;
}
~~~

## cdav

`cdav` is a command line CalDAV and CardDAV client with support for multiple calendars, address books,
time zones. Add, edit, delete list events and contacts.

### Screnshot

![Screenshot](rofi.png?raw=true)

~~~sh
cdav -s month -Ecf | rofi -dmenu -markup-rows
~~~

Creating a simple address book with Rofi:

![Addressbook](addressbook.png?raw=true)

~~~sh
open "mailto:$(cdav -s contacts | rofi -dmenu)"
~~~

### Usage

~~~
cdav [<options>] [urls...]
~~~

### Options

~~~
  -s, --show <what>         Show month, month2, week, day, contacts or discover
                            or custom range (start)-(end), e.g. 20221101-20230101
  -a, --add <what>          Add event, todo, journal or contact
  -d, --delete <href>       Delete event or contact
  -e, --edit <href>         Edit event or contact
  -g, --get <href>          Get event or contact
  -i, --import <filename>   Import event or contact
                            Read standard in when filename is -
  -c, --calendar            Show month calendar
  -E, --events              Show events as list
  -F, --future              Show only future events
  -T, --todos               Get TODOs instead of events
  -J, --journal             Get Journal instead of events
  -f, --markup-format       Format output suitable for Pango
  -A, --notify-alarm        Make a notifications for alarms
  -r, --features            Discover features
  -P, --user-principal      Discover user principal
  -L, --list                Discover list
  -t, --item-template <t>   Template to display item
  -S, --no-ssl-verify       Don't verify SSL
  -C, --config <config>     Use other config file
  -u, --username <value>    Auth Username
  -p, --password <value>    Auth Password
  -H, --charset <value>     Charset (default utf8)
  -Z, --timezone <value>    Set timezone
  -h, --help                Show help
  -v, --version             Show version
~~~

### Examples

~~~
  Show future events of the current month
   $ cdav -s month -EF
  Show all events between 1st Nov. 2022 and 1st Jan. 2023
   $ cdav -s "20221101-20230101" -E
  Show all Contacts
   $ cdav -s contacts
  Import Event event.ics
   $ cdav -i event.ics
  Import Contact some_person.vcf
   $ cdav -i some_person.vcf
  Show href of events in the current month
   $ cdav -s month -E -t "\${href}"
  Search for events between 1.11.2022 and 1.1.2023 with name Urlaub
   $ cdav -s "20221101-20230101" -E -t "${name}" | grep Urlaub
~~~

### Config file

It looks for config files in:

1. `~/.config/cdav/config.conf`
2. `~/.config/cdav.conf`
3. `~/.cdav.conf`

It should look similar to this:

~~~
# First calendar is the default and used for adding, importing events
calendar-1=http://localhost:8000/dav.php/calendars/sa/default/
calendar-2=http://localhost:8000/dav.php/calendars/sa/feiertage-2000---2035/
# Same as for calendar, first is the default
addressbook-1=http://localhost:8000/dav.php/addressbooks/sa/default/
username=sa
password=top_secret!
verify_ssl=true
event_template=${name}: ${start(%d.%m.%Y)} ${start(%H:%M)}-${end(%H:%M)}
contact_template=${formatted_name} <${email}>
~~~

All these options can be passed via command line as well.

### Envronment variables

* `TZ` Sets the time zone.
* `NO_COLOR` Don't print ANSI escape sequences when writing to the terminal.

