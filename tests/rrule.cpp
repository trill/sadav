/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <catch2/catch.hpp>
#include <event.h>
#include <iostream>
#include <vector>

using namespace sa::dav;

TEST_CASE("RRule weekly")
{
    static constexpr const char* src = R"(VERSION:2.0
PRODID:-//K Desktop Environment//NONSGML libkcal 4.3//EN
BEGIN:VEVENT
UID:76829e62-7c98-4255-807d-97138e7202bf
DTSTAMP:20220923T161557Z
CREATED:20220905T020803Z
DTSTART;TZID=Europe/Vienna:20220909T183000
DTEND;TZID=Europe/Vienna:20220909T200000
SUMMARY:Something weekly
RRULE:FREQ=WEEKLY;BYDAY=TH
EXDATE:20220930T003000Z,20220930T023000Z,20220930T043000Z,20220930T063000Z
SEQUENCE:1
TRANSP:OPAQUE
END:VEVENT)";

    Event event("", "", "Test", Color::FromString("5555ff"));
    REQUIRE(event.Parse(src));
    REQUIRE(event.IsRecurring());
    DateTime start;
    REQUIRE(start.Parse("20221201T000000Z"));
    DateTime end;
    REQUIRE(end.Parse("20221231T235959Z"));

    std::vector<Event> events;
    event.AddRecurring(start, end, events);
    for (const auto& e : events)
        REQUIRE(e.start_.WeekDay() == DateTime::Weekday::Thursday);

    REQUIRE(events.size() == 5);

}
