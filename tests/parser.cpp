/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <catch2/catch.hpp>
#include <iostream>
#include <sstream>
#include <parseutils.h>
#include <event.h>

using namespace sa::dav;

TEST_CASE("Multiline field")
{
    static constexpr const char* src = R"(BEGIN:VCARD
VERSION:4.0
N:Some;Name;II;;
PHOTO;ENCODING=b;TYPE=png:iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAAC
 XBIWXMAAA7EAAAOxAGVKw4bAAAFFUlEQVR4nO3dsY7cVBiG4TMR3AJVmu1pgpAiLmB2q6SOlJKa
 Og0FQmm4iJSg7VPt+AqQIgJSSqQ0WwG5AqQMBaL99jc6R2c8+zy1ZXtm59Va+u3j1gAAAAAAAAA
 AAAAAAAAAAABYbTf7BLZgv98fZ5/DCMuy+Pvf4cHsE4BTJhAIBAKBQCAQCAQCgUAgEAgEAoFAcJ
 aT1HOdfJ+6c5zMn90HGhXH4fBTccvf6zs9vvlf5xL9+k1ps8sX+/7HbucXiUssCAQCgUAgEAgEA
 oFAIBAIBAKBQCAQCDYz9VwzIe8+9T6+ae34XfXwdR8+9N9n1fvaZsd39V1eXden81uZuH8y+wSY
 5NPZJ7ANLrEgEAgEAoFAIBAIBAKBQCAQCAQCwfRpZu9nyA83T1vbfXn3hmufB+89SZ85Rf/PbW2
 z49v6LtdM0ytmT9ynHnzd7SPf13dc/fH/VVvg4N6bHNLMSFxiQSAQCAQCgUAgEAgEAoFAIBAIBA
 KBQLCdZ9LX3BpSnJBfPu//CoDDj0v3fU73sLbZ1aBXKsy0nUAGWZYz/EFPVP0+9/ttxOQSCwKBQ
 CAQCAQCgUAgEAgEAoFAIBAMGRT2XohhlZXPmXcfWL0fMHi86L/LVYqvSmhtzACw+nsa8ez69El6
 eTGGP7/qf+wBt4VcPt93n84f/+i6u3VuW9s9rm16/Ln2uUe9c2QEl1gQCAQCgUAgEAgEAoFAIBA
 IBAKBQCAQTJ+kc7cRi0vcPBtwF8G323jOfA2BbMCQhSWe9d/liPOcvbiDSywIBAKBQCAQCAQCgU
 AgEAgEAoFAcL8HhbfzDr12ANZ7CHd1ve8+TV/zmbayqn73VSBaW7eqyeHmaW3DlauVVIxYYGHNj
 6S6aMSaW01mTrPXLIJR/UwrP0/337NLLAgEAoFAIBAIBAKBQCAQCAQCgUAgEAim3mpy82xp7fjL
 zFMo2e/37fBDcer9YvDJ3OHjq9p2V9f97yIYofzd/93a7nFb816a0tT9ft+LtcbD2SdwP414h8s
 aLrEgEAgEAoFAIBAIBAKBQCAQCAQCwdRAZr8kHu4yf5L+24fadibZJbsnxQ2vh57G2XCJBYFAIB
 AIBAKBQCAQCAQCgUAgEAgEAsH0Sfrli3t8u8n72SewAZO/oyGBLMtSWjFizXtEtrACx1rHd7PP4
 PTN/o5cYkEgEAgEAoFAIBAIBAKBQCAQCASC6ZP03qovvW/tPIePm3A7+wTqzi6Q1upL5l8+7/+O
 jBHL9d+0+j6Pr/see7/f//sel87Hru5zNpdYEAgEAoFAIBAIBAKBQCAQCAQCgUAgEEydpC/Lsqs
 +l77mFpKqw8ultZeFDS/mTn13j2rbHd+OPY9Z6u+RKf+dSmsmtHYCt5qMWOCh7KL7Hscovhtl97
 D/rSazXV2vuh2o/MOvcokFgUAgEAgEAoFAIBAIBAKBQCAQCARyRs5tSHgKpk/Sq6oT99bacfdZb
 cOPr2rb7Z4UjzzZ7lFruy+qW8+9febB19Utl9YGTMir/AeBQCAQCAQCgUAgEAgEAoFAIBAIBAKB
 YDOT9BGqiwEcPi9MnQc+3358PWThgtrelqVdtf4LZoxYYGGEqQcfpPviDtV3ZIy6JWXEe0xOwCZ
 +ey6xIBAIBAKBQCAQCAQCgUAgEAgEAoFAOrJowvnZxDRzoP6vVBjnvv+tpvAfBAKBQCAQCAQCgU
 AgEAgEAoFAIBAIBAKBbIMpOgAAAAAAAABwAv4BkxT7G0HLevwAAAAASUVORK5CYII=
PRODID:-//Inverse inc.//SOGo Connector 1.0//EN
END:VCARD)";
    std::stringstream ss(src);
    std::string line;
    int i = 0;
    while (NextField(ss, line))
    {
//        std::cout << line << std::endl;
        ++i;
    }
    REQUIRE(i == 6);
}

TEST_CASE("ParseDuration")
{
    SECTION("P7W")
    {
        auto v = ParseDuration("P7W");
        REQUIRE(v.has_value());
        REQUIRE(*v == 7 * SECONDS_PER_WEEK);
    }
    SECTION("-P7W")
    {
        auto v = ParseDuration("-P7W");
        REQUIRE(v.has_value());
        REQUIRE(*v == -(7 * SECONDS_PER_WEEK));
    }
    SECTION("P15DT5H0M20S")
    {
        auto v = ParseDuration("P15DT5H0M20S");
        REQUIRE(v.has_value());
        REQUIRE(*v == 15 * SECONDS_PER_DAY + 5 * SECONDS_PER_HOUR + 20);
    }
}

TEST_CASE("Alarm")
{
    static constexpr const char* src = R"(VERSION:2.0
PRODID:-//sa//libsadav 1.3.0//EN
BEGIN:VEVENT
UID:8ef1f99f-0945-40c9-80dd-f70a07c0879a
DTSTAMP:20221203T050500Z
CREATED;TZID=Europe/Vienna:20221203T060500
DTSTART;TZID=Europe/Vienna:20221203T060500
DTEND;TZID=Europe/Vienna:20221203T070500
SUMMARY:Test Alarm
BEGIN:VALARM
TRIGGER:-PT30M
REPEAT:2
DURATION:PT15M
ACTION:DISPLAY
DESCRIPTION:Breakfast meeting with executive\n
 team at 8:30 AM EST.
END:VALARM
END:VEVENT)";
    Event event("", "", "", {});
    REQUIRE(event.Parse(src));
    REQUIRE(event.alarms_.size() == 1);
    const auto& al = event.alarms_[0];
    auto trigger = al.GetTrigger(event);
    REQUIRE(trigger.has_value());
    // 30 Min before start
    REQUIRE(trigger->trigger.time_ == event.start_.time_ - 30 * 60);
    // Relevant value is start
    REQUIRE(trigger->relevant.time_ == event.start_.time_);
}
