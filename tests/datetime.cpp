/**
 * Copyright (c) 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <catch2/catch.hpp>
#include <date/date.h>
#include <date/tz.h>
#include <datetime.h>

using namespace sa::dav;

TEST_CASE("Parse")
{
    DateTime nlocal = DateTime::Now();
    std::string strlocal = nlocal.ToString();
    DateTime nutc = DateTime::Now();
    nutc.tz_ = "UTC";
    std::string strutc = nutc.ToString();

    DateTime nny = DateTime::Now();
    nny.tz_ = "America/New_York";
    std::string strny = nny.ToString();
}
