/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "configfile.h"
#include <iostream>
#include <fstream>

ConfigFile::ConfigFile() = default;

ConfigFile::~ConfigFile() = default;

bool ConfigFile::Load(const std::string& filename)
{
    entires_.clear();

    std::ifstream in(filename);
    if (!in.is_open())
        return false;

    std::string line;
    while (std::getline(in, line))
    {
        line = sa::dav::Trim<char>(line, " \t");
        if (line.empty() || line[0] == '#')
            continue;

        auto pos = line.find('=');
        if (pos == std::string::npos)
            continue;
        entires_.emplace(line.substr(0, pos), line.substr(pos + 1));
    }

    return true;
}
