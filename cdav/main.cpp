#include <addressbook.h>
#include <calendar.h>
#include <getopt.h>
#include <parseutils.h>
#include <url.h>
#include <cassert>
#include <filesystem>
#include <iostream>
#include <ostream>
#include "configfile.h"
#include <discover.h>
#include <config.h>
#include "display.h"
#include "fileutils.h"
#include "version.h"
#ifdef HAVE_LIBNOTIFY
#include <libnotify/notify.h>
#endif

static constexpr const char* CONFIG_PATHS[] = {
    "~/.config/cdav/config.conf",
    "~/.config/cdav.conf",
    "~/.cdav.conf",
};

enum class Action
{
    Show,
    Add,
    Delete,
    Edit,
    Get,
    Import
};

Action action = Action::Show;
std::string what;
std::string username;
std::string password;
std::vector<std::string> posargs;
bool verifySsl = true;
bool calendar = false;
bool showEvents = false;
bool markup = false;
bool futureOnly = false;
#ifdef HAVE_LIBNOTIFY
bool notifyAlarm = false;
#endif
bool discoverFeatures = false;
bool discoverUserPrincipal = false;
bool discoverList = false;
std::string itemTemplate;
sa::dav::Event::Type eventType = sa::dav::Event::Type::Event;
std::string charset = "utf8";
sa::dav::DateTime start;
sa::dav::DateTime end;

template<typename ContainerT, typename PredicateT>
bool DeleteIf(ContainerT& items, const PredicateT& predicate)
{
    bool result = false;
    for (auto it = items.begin(); it != items.end();)
    {
        if (predicate(*it))
        {
            result = true;
            it = items.erase(it);
        }
        else
            ++it;
    }
    return result;
}

template<typename T>
static std::shared_ptr<T> GetClient(const std::string url, const ConfigFile& cfg)
{
    auto result = std::make_shared<T>(url, username, password);
    result->onError_ = [](const std::string& category, const std::string& message)
    { std::cerr << std::format("[{}] {}", category, message) << std::endl; };
    if (!verifySsl)
        result->verifySsl_ = false;
    else
        result->verifySsl_ = cfg.Get("verify_ssl", true);
    result->auth_ = cfg.Get("auth", sa::dav::Client::Auth::Try);
    return result;
}

static std::vector<std::string> GetURLs(const std::string& ident, const ConfigFile& cfg)
{
    if (!posargs.empty())
        return posargs;
    return cfg.GetList<std::string>(ident);
}

static std::string GetTempFile(const std::string& fn)
{
    namespace fs = std::filesystem;
    fs::path result = fs::temp_directory_path();
    result /= fn;
    return result.string();
}

static bool Edit(const std::string& filename)
{
    char* editor = getenv("VISUAL");
    if (editor == nullptr)
        editor = getenv("EDITOR");
    if (editor == nullptr)
    {
        std::cerr << "No editor found please set the EDITOR environment valiable" << std::endl;
        return false;
    }
    std::string cmd = std::format("{} {}", editor, filename);
    auto ret = system(cmd.c_str());
    return ret == 0;
}

static std::string ReadStdIn()
{
    std::string line;
    std::stringstream ss;
    while (std::getline(std::cin, line))
    {
        ss << line << '\n';
    }
    return ss.str();
}

static void CleanEvents(std::vector<sa::dav::Event>& events)
{
    if (!futureOnly)
        return;
    sa::dav::DateTime now = sa::dav::DateTime::Now();
    DeleteIf(events,
        [&](const sa::dav::Event& current) -> bool
        {
            if (!current.start_.Empty() && current.start_ < now)
                return true;
            if (!current.due_.Empty() && current.due_ < now)
                return true;
            if (!current.end_.Empty() && current.end_ < now)
                return true;
            return false;
        });
}

static void SortEvents(std::vector<sa::dav::Event>& events)
{
    std::sort(events.begin(),
        events.end(),
        [](const sa::dav::Event& a, const sa::dav::Event& b) -> bool { return a.start_ < b.start_; });
}

static void NotifiyAlarm(std::vector<sa::dav::Event>& events)
{
#ifdef HAVE_LIBNOTIFY
    if (!notifyAlarm)
        return;
    notify_init("cdav");
    for (const auto& e : events)
    {
        e.GetCurrentAlarams([&](const sa::dav::Alarm& alarm, const sa::dav::Alarm::Trigger&) {
            if (alarm.GetAction() != sa::dav::Alarm::Action::Display)
                return;
            std::string title = alarm.GetSummary(e);
            std::string descr = alarm.GetDescription(e);
            if (title.empty())
                title = "Alarm";
            NotifyNotification* notif = notify_notification_new(title.c_str(), descr.empty() ? nullptr : descr.c_str(), nullptr);
            notify_notification_show(notif, nullptr);
        });
    }
    notify_uninit();
#else
    (void)events;
#endif
}

static bool GetCalendar(const ConfigFile& cfg)
{
    if (itemTemplate.empty())
        itemTemplate = cfg.Get<std::string>("event_template", "");

    auto getEvents = [&](const sa::dav::DateTime& start, const sa::dav::DateTime& end, std::vector<sa::dav::Event>& events) -> bool
    {
        auto urls = GetURLs("calendar", cfg);
        for (const auto& cal : urls)
        {
            auto client = GetClient<sa::dav::Calendar>(cal, cfg);
            if (!client->GetEvents(start, end, eventType, events))
                return false;
        }
        CleanEvents(events);
        SortEvents(events);
        return true;
    };

    if (what == "month")
    {
        sa::dav::DateTime start = sa::dav::DateTime::Now();
        start.SetDate(start.Year(), start.Month(), 1);
        start.SetTime(0, 0, 0);
        sa::dav::DateTime end = start;
        end.Add(0, 1, -1);
        end.SetTime(23, 59, 59);

        std::vector<sa::dav::Event> events;
        if (!getEvents(start, end, events))
            return false;
        DisplayEvents(start, end, events, itemTemplate, calendar, showEvents, markup);
        NotifiyAlarm(events);
        return true;
    }
    if (what == "month2")
    {
        sa::dav::DateTime start = sa::dav::DateTime::Now();
        start.SetDate(start.Year(), start.Month(), 1);
        start.SetTime(0, 0, 0);
        sa::dav::DateTime end = start;
        end.Add(0, 2, -1);
        end.SetTime(23, 59, 59);

        std::vector<sa::dav::Event> events;
        if (!getEvents(start, end, events))
            return false;
        DisplayEvents(start, end, events, itemTemplate, calendar, showEvents, markup);
        NotifiyAlarm(events);
        return true;
    }
    if (what == "week")
    {
        sa::dav::DateTime now = sa::dav::DateTime::Now();
        auto start = sa::dav::DateTime::WeekdayOfMonth(sa::dav::DateTime::Weekday::Sunday, 1, now);
        start.SetTime(0, 0, 0);
        sa::dav::DateTime end = start;
        end.Add(0, 0, 6);
        end.SetTime(23, 59, 59);

        std::vector<sa::dav::Event> events;
        if (!getEvents(start, end, events))
            return false;
        DisplayEvents(start, end, events, itemTemplate, false, showEvents, markup);
        NotifiyAlarm(events);
        return true;
    }
    if (what == "day")
    {
        sa::dav::DateTime start = sa::dav::DateTime::Now();
        start.SetTime(0, 0, 0);
        sa::dav::DateTime end = start;
        end.SetTime(23, 59, 59);

        std::vector<sa::dav::Event> events;
        if (!getEvents(start, end, events))
            return false;
        DisplayEvents(start, end, events, itemTemplate, false, showEvents, markup);
        NotifiyAlarm(events);
        return true;
    }
    if (!start.Empty() && !end.Empty())
    {
        std::vector<sa::dav::Event> events;
        if (!getEvents(start, end, events))
            return false;
        DisplayEvents(start, end, events, itemTemplate, false, showEvents, markup);
        NotifiyAlarm(events);
        return true;
    }
    return false;
}

static bool GetContacts(const ConfigFile& cfg)
{
    std::vector<sa::dav::Contact> contacts;
    auto urls = GetURLs("addressbook", cfg);
    for (const auto& cal : urls)
    {
        auto client = GetClient<sa::dav::AddressBook>(cal, cfg);
        if (!client->GetContacts(contacts))
            return false;
    }

    if (itemTemplate.empty())
        itemTemplate = cfg.Get<std::string>("contact_template", "");
    DisplayContacts(contacts, itemTemplate);
    return true;
}

static bool Discover(const ConfigFile& cfg)
{
    auto urls = GetURLs("addressbook", cfg);

    for (const auto& url : urls)
    {
        std::cout << url << std::endl;
        auto client = GetClient<sa::dav::Discover>(url, cfg);

        if (discoverFeatures)
        {
            auto feat = client->GetFeatures();
            for (const auto& f : feat)
                std::cout << f << std::endl;
        }
        if (discoverUserPrincipal)
        {
            auto up = client->UserPrincipal();
            std::cout << up << std::endl;
        }
        if (discoverList)
        {
            auto lst = client->List();
            for (const auto& f : lst)
                std::cout << f.first << ": " << f.second << std::endl;
        }
    }
    return true;
}

static bool ActionShow(const ConfigFile& cfg)
{
    if (what.empty())
    {
        std::cerr << "Show what?" << std::endl;
        return false;
    }

    if (what == "month" || what == "month2" || what == "week" || what == "day")
        return GetCalendar(cfg);

    if (what == "contacts")
        return GetContacts(cfg);

    if (what == "discover")
        return Discover(cfg);

    if (what.find('-') != std::string::npos)
    {
        // Custom range startdate-enddate
        auto parts = sa::dav::Split(what, "-", false, false);
        if (parts.size() == 2)
        {
            if (start.Parse(parts[0]) && end.Parse(parts[1]))
            {
                return GetCalendar(cfg);
            }
        }
    }

    std::cerr << "Unknown what " << what << std::endl;
    return false;
}

static bool AddContact(const ConfigFile& cfg)
{
    auto urls = GetURLs("addressbook", cfg);
    if (urls.empty())
    {
        std::cerr << "URL is empty" << std::endl;
        return false;
    }
    const auto& url = urls.front();

    auto client = GetClient<sa::dav::AddressBook>(url, cfg);
    auto contact = client->New(sa::dav::ObjectType::Contact);
    assert(contact);
    std::string tmpFile = GetTempFile(contact->GetFilename());
    std::string origContents = contact->ToString();
    if (!sa::dav::WriteFile(tmpFile, origContents))
    {
        std::cerr << "Unable to create file " << tmpFile << std::endl;
        return false;
    }

    if (!Edit(tmpFile))
        return false;
    std::string content = sa::dav::ReadFile(tmpFile);
    if (content.empty() || origContents == content)
        return false;

    if (!contact->Parse(content))
    {
        std::cerr << "Unable to parse content, review it " << tmpFile << std::endl;
        return false;
    }

    if (!client->Put(*contact, charset))
        return false;
    std::cout << "Added " << contact->GetHref() << std::endl;
    return true;
}

static bool AddEvent(const ConfigFile& cfg, sa::dav::Event::Type type)
{
    auto urls = GetURLs("calendar", cfg);
    if (urls.empty())
    {
        std::cerr << "URL is empty" << std::endl;
        return false;
    }
    const auto& url = urls.front();

    auto client = GetClient<sa::dav::Calendar>(url, cfg);

    sa::dav::ObjectType otype = sa::dav::ObjectType::Unknown;
    switch (type)
    {
    case sa::dav::Event::Type::Event:
        otype = sa::dav::ObjectType::Event;
        break;
    case sa::dav::Event::Type::Todo:
        otype = sa::dav::ObjectType::Todo;
        break;
    case sa::dav::Event::Type::Journal:
        otype = sa::dav::ObjectType::Journal;
        break;
    }

    auto event = client->New(otype);
    assert(event);
    static_cast<sa::dav::Event&>(*event).created_ = sa::dav::DateTime::Now();
    std::string tmpFile = GetTempFile(event->GetFilename());
    std::string origContents = event->ToString();
    if (!sa::dav::WriteFile(tmpFile, origContents))
    {
        std::cerr << "Unable to create file " << tmpFile << std::endl;
        return false;
    }

    if (!Edit(tmpFile))
        return false;
    std::string content = sa::dav::ReadFile(tmpFile);
    if (content.empty() || origContents == content)
        return false;

    if (!event->Parse(content))
    {
        std::cerr << "Unable to parse content, review it " << tmpFile << std::endl;
        return false;
    }

    if (!client->Put(*event, charset))
        return false;
    std::cout << "Added " << event->GetHref() << std::endl;
    return true;
}

static bool ActionAdd(const ConfigFile& cfg)
{
    if (what.empty())
    {
        std::cerr << "Add what?" << std::endl;
        return false;
    }
    if (what == "contact")
        return AddContact(cfg);
    if (what == "event")
        return AddEvent(cfg, sa::dav::Event::Type::Event);
    if (what == "todo")
        return AddEvent(cfg, sa::dav::Event::Type::Todo);
    if (what == "journal")
        return AddEvent(cfg, sa::dav::Event::Type::Journal);

    std::cerr << "Unknown what " << what << std::endl;
    return false;
}

static bool ActionDelete(const ConfigFile& cfg)
{
    if (what.empty())
    {
        std::cerr << "href is empty" << std::endl;
        return false;
    }

    auto tryDelete = [&](const std::string& ident) -> bool
    {
        auto urls = GetURLs(ident, cfg);
        if (urls.empty())
        {
            return false;
        }
        for (const auto& url : urls)
        {
            auto client = GetClient<sa::dav::Client>(url, cfg);
            if (client->Delete(what))
            {
                std::cout << "Deleted " << what << std::endl;
                return true;
            }
        }
        return false;
    };

    if (tryDelete("calendar"))
        return true;
    if (tryDelete("addressbook"))
        return true;
    return false;
}

static bool ActionGet(const ConfigFile& cfg)
{
    if (what.empty())
    {
        std::cerr << "href is empty" << std::endl;
        return false;
    }

    auto tryGet = [&](const std::string& ident) -> bool
    {
        auto urls = GetURLs(ident, cfg);
        if (urls.empty())
        {
            return false;
        }
        for (const auto& url : urls)
        {
            auto client = GetClient<sa::dav::Client>(url, cfg);
            auto result = client->Get(what);
            if (result)
            {
                std::cout << result->ToString();
                return true;
            }
        }
        return false;
    };

    if (tryGet("calendar"))
        return true;
    if (tryGet("addressbook"))
        return true;
    return false;
}

static bool ActionEdit(const ConfigFile& cfg)
{
    if (what.empty())
    {
        std::cerr << "href is empty" << std::endl;
        return false;
    }

    bool error = false;
    auto tryEdit = [&](const std::string& ident) -> bool
    {
        auto urls = GetURLs(ident, cfg);
        if (urls.empty())
            return false;

        for (const auto& url : urls)
        {
            auto client = GetClient<sa::dav::Client>(url, cfg);
            auto object = client->Get(what);
            if (object)
            {
                std::string tmpFile = GetTempFile(object->GetFilename());
                std::string origContents = object->ToString();
                if (!sa::dav::WriteFile(tmpFile, origContents))
                {
                    error = true;
                    std::cerr << "Unable to create file " << tmpFile << std::endl;
                    return false;
                }
                if (!Edit(tmpFile))
                    return false;
                std::string content = sa::dav::ReadFile(tmpFile);
                if (content.empty())
                {
                    error = true;
                    return false;
                }
                if (origContents == content)
                {
                    std::cerr << "No changes " << object->GetHref() << std::endl;
                    return true;
                }

                if (!object->Parse(content))
                {
                    std::cerr << "Unable to parse content, review it " << tmpFile << std::endl;
                    error = true;
                    return false;
                }

                if (!client->Put(*object, charset))
                {
                    error = true;
                    return false;
                }
                std::cout << "Updated " << object->GetHref() << std::endl;
                return true;
            }
        }
        return false;
    };

    if (tryEdit("calendar"))
        return true;
    if (error)
        return false;
    if (tryEdit("addressbook"))
        return true;
    return false;
}

static bool ActionImport(const ConfigFile& cfg)
{
    std::string contents;
    if (what == "-")
        contents = ReadStdIn();
    else
        contents = sa::dav::ReadFile(what);

    if (contents.empty())
    {
        std::cerr << "Can not read file " << what << std::endl;
        return false;
    }

    std::shared_ptr<sa::dav::Client> client;
    std::shared_ptr<sa::dav::Object> object;

    auto type = sa::dav::DetectObjectType(contents);
    if (type == sa::dav::ObjectType::Unknown)
    {
        std::cerr << "Unknown object type" << std::endl;
        return false;
    }
    if (type == sa::dav::ObjectType::Event || type == sa::dav::ObjectType::Todo || type == sa::dav::ObjectType::Journal)
    {
        auto urls = GetURLs("calendar", cfg);
        if (urls.empty())
        {
            std::cerr << "URL is empty" << std::endl;
            return false;
        }
        // Using the first
        const auto& url = urls.front();
        client = GetClient<sa::dav::Calendar>(url, cfg);
        object = client->New(type);
    }
    else if (type == sa::dav::ObjectType::Contact)
    {
        auto urls = GetURLs("addressbook", cfg);
        if (urls.empty())
        {
            std::cerr << "URL is empty" << std::endl;
            return false;
        }
        const auto& url = urls.front();
        client = GetClient<sa::dav::AddressBook>(url, cfg);
        object = client->New(type);
    }
    else
    {
        assert(false);
    }

    assert(client);
    assert(object);

    if (!object->Parse(contents))
    {
        std::cerr << "Unable to parse file " << what << std::endl;
        return false;
    }
    if (!client->Put(*object, charset))
        return false;
    std::cout << "Imported " << object->GetHref() << std::endl;
    return true;
}

static void ShowInfo()
{
    std::cout << "libsadav and cdav" << std::endl;
    std::cout << "CalDAV/CardDAV client library and command line CalDAV/CardDAV client" << std::endl;
    std::cout << "Version: " << SADAV_VERSION_STRING << std::endl << std::endl;
    std::cout << "Copyright (C) 2022 Stefan Ascher" << std::endl;
    std::cout << "This is free software with ABSOLUTELY NO WARRANTY." << std::endl;
}

static void ShowHelp()
{
    std::cout << "Usage: cdav [<options>]  [urls...]" << std::endl << std::endl;
    std::cout << "Options:" << std::endl;
    std::cout << "  -s, --show <what>         Show month, month2, week, day, contacts or discover" << std::endl;
    std::cout << "                            or custom range (start)-(end), e.g. 20221101-20230101" << std::endl;
    std::cout << "  -a, --add <what>          Add event, todo, journal or contact" << std::endl;
    std::cout << "  -d, --delete <href>       Delete event or contact" << std::endl;
    std::cout << "  -e, --edit <href>         Edit event or contact" << std::endl;
    std::cout << "  -g, --get <href>          Get event or contact" << std::endl;
    std::cout << "  -i, --import <filename>   Import event or contact" << std::endl;
    std::cout << "                            Read standard in when filename is -" << std::endl;

    std::cout << "  -c, --calendar            Show month calendar" << std::endl;
    std::cout << "  -E, --events              Show events as list" << std::endl;
    std::cout << "  -F, --future              Show only future events" << std::endl;
    std::cout << "  -T, --todos               Get TODOs instead of events" << std::endl;
    std::cout << "  -J, --journal             Get Journal instead of events" << std::endl;
    std::cout << "  -f, --markup-format       Format output suitable for Pango" << std::endl;
#ifdef HAVE_LIBNOTIFY
    std::cout << "  -A, --notify-alarm        Make a notifications for alarms" << std::endl;
#endif

    std::cout << "  -r, --features            Discover features" << std::endl;
    std::cout << "  -P, --user-principal      Discover user principal" << std::endl;
    std::cout << "  -L, --list                Discover list" << std::endl;

    std::cout << "  -t, --item-template <t>   Template to display item" << std::endl;
    std::cout << "  -S, --no-ssl-verify       Don't verify SSL" << std::endl;
    std::cout << "  -C, --config <config>     Use other config file" << std::endl;
    std::cout << "  -u, --username <value>    Auth Username" << std::endl;
    std::cout << "  -p, --password <value>    Auth Password" << std::endl;
    std::cout << "  -H, --charset <value>     Charset (default utf8)" << std::endl;
    std::cout << "  -Z, --timezone <value>    Set timezone" << std::endl;

    std::cout << "  -h, --help                Show help" << std::endl;
    std::cout << "  -v, --version             Show version" << std::endl;
    std::cout << std::endl;

    std::cout << "Examples:" << std::endl;
    std::cout << "  Show future events of the current month" << std::endl;
    std::cout << "   $ cdav -s month -EF" << std::endl;
    std::cout << "  Show all events between 1st Nov. 2022 and 1st Jan. 2023" << std::endl;
    std::cout << "   $ cdav -s \"20221101-20230101\" -E" << std::endl;
    std::cout << "  Show all Contacts" << std::endl;
    std::cout << "   $ cdav -s contacts" << std::endl;
    std::cout << "  Import Event event.ics" << std::endl;
    std::cout << "   $ cdav -i event.ics" << std::endl;
    std::cout << "  Import Contact some_person.vcf" << std::endl;
    std::cout << "   $ cdav -i some_person.vcf" << std::endl;
    std::cout << "  Show href of events in the current month" << std::endl;
    std::cout << R"(   $ cdav -s month -E -t "\${href}")" << std::endl;
    std::cout << "  Search for events between 1.11.2022 and 1.1.2023 with name Urlaub" << std::endl;
    std::cout << R"(   $ cdav -s "20221101-20230101" -E -t "\${name}" | grep Urlaub)" << std::endl;
}

int main(int argc, char** argv)
{
    // clang-format off
    static struct option longOptions[] = {
        // Action
        { "show",           required_argument, nullptr, 's' },
        { "add",            required_argument, nullptr, 'a' },
        { "delete",         required_argument, nullptr, 'd' },
        { "edit",           required_argument, nullptr, 'e' },
        { "get",            required_argument, nullptr, 'g' },
        { "import",         required_argument, nullptr, 'i' },

        // Display options
        { "calendar",       no_argument,       nullptr, 'c' },
        { "events",         no_argument,       nullptr, 'E' },
        { "future",         no_argument,       nullptr, 'F' },
        { "todos",          no_argument,       nullptr, 'T' },
        { "journal",        no_argument,       nullptr, 'J' },
        { "markup-format",  no_argument,       nullptr, 'f' },
#ifdef HAVE_LIBNOTIFY
        { "notify-alarm",   no_argument,       nullptr, 'A' },
#endif

        // Discover
        { "features",       no_argument,       nullptr, 'r' },
        { "user-principal", no_argument,       nullptr, 'P' },
        { "list",           no_argument,       nullptr, 'L' },

        // Options
        { "item-template",  required_argument, nullptr, 't' },
        { "no-ssl-verify",  no_argument,       nullptr, 'S' },
        { "config",         required_argument, nullptr, 'C' },
        { "username",       required_argument, nullptr, 'u' },
        { "password",       required_argument, nullptr, 'p' },
        { "charset",        required_argument, nullptr, 'H' },
        { "timezone",       required_argument, nullptr, 'Z' },

        { "help",           no_argument,       nullptr, 'h' },
        { "version",        no_argument,       nullptr, 'v' },
        { nullptr, 0, nullptr, 0 }
    };
    // clang-format on

    if (argc == 1)
    {
        ShowInfo();
        return 0;
    }
    int optionIndex = 0;
    std::string configFile;
    std::string timezone;

    if (auto* tz = getenv("TZ"))
        timezone = tz;

    static constexpr const char* SHORT_OPTS = "s:a:d:e:g:i:cEFTJfrPLt:SC:u:p:H:Z:hv"
#ifdef HAVE_LIBNOTIFY
            "A"
#endif
            ;

    int c;
    while ((c = getopt_long(argc, argv, SHORT_OPTS, longOptions, &optionIndex)) != -1)
    {
        switch (c)
        {
        case 's':
            what = optarg;
            action = Action::Show;
            break;
        case 'a':
            what = optarg;
            action = Action::Add;
            break;
        case 'd':
            what = optarg;
            action = Action::Delete;
            break;
        case 'e':
            what = optarg;
            action = Action::Edit;
            break;
        case 'g':
            what = optarg;
            action = Action::Get;
            break;
        case 'i':
            what = optarg;
            action = Action::Import;
            break;

        case 'c':
            calendar = true;
            break;
        case 'E':
            showEvents = true;
            break;
        case 'F':
            futureOnly = true;
            break;
        case 'T':
            eventType = sa::dav::Event::Type::Todo;
            break;
        case 'J':
            eventType = sa::dav::Event::Type::Journal;
            break;
        case 'f':
            markup = true;
            break;
#ifdef HAVE_LIBNOTIFY
        case 'A':
            notifyAlarm = true;
            break;
#endif

        case 'r':
            discoverFeatures = true;
            break;
        case 'P':
            discoverUserPrincipal = true;
            break;
        case 'L':
            discoverList = true;
            break;

        case 't':
            itemTemplate = optarg;
            break;
        case 'S':
            verifySsl = false;
            break;
        case 'C':
            configFile = optarg;
            break;
        case 'u':
            username = optarg;
            break;
        case 'p':
            password = optarg;
            break;
        case 'H':
            charset = optarg;
            break;
        case 'Z':
            timezone = optarg;
            break;

        case 'h':
            ShowHelp();
            return 0;
        case 'v':
            std::cout << SADAV_VERSION_STRING << std::endl;
            return 0;
        }
    }

    for (int i = optind; i < argc; ++i)
        posargs.emplace_back(argv[i]);

    ConfigFile cfg;
    if (configFile.empty())
    {
        for (const auto& p : CONFIG_PATHS)
        {
            if (cfg.Load(sa::dav::ExpandPath(p)))
                break;
        }
    }
    else
    {
        if (!cfg.Load(configFile))
        {
            std::cerr << "Unable lo load config file " << configFile << std::endl;
            return 1;
        }
    }

    if (!timezone.empty())
        sa::dav::Config::Instance().SetTimezone(timezone);
    else
        sa::dav::Config::Instance().SetTimezone(cfg.Get<std::string>("timezone", sa::dav::Config::Instance().Timezone()));
    if (username.empty())
        username = cfg.Get("username", "");
    if (password.empty())
        password = cfg.Get("password", "");
#ifdef HAVE_LIBNOTIFY
    if (!notifyAlarm)
        notifyAlarm = cfg.Get("notify_alarm", false);
#endif

    if (action == Action::Show)
        return ActionShow(cfg) ? 0 : 1;
    if (action == Action::Add)
        return ActionAdd(cfg) ? 0 : 1;
    if (action == Action::Delete)
        return ActionDelete(cfg) ? 0 : 1;
    if (action == Action::Get)
        return ActionGet(cfg) ? 0 : 1;
    if (action == Action::Edit)
        return ActionEdit(cfg) ? 0 : 1;
    if (action == Action::Import)
        return ActionImport(cfg) ? 0 : 1;

    std::cerr << "Unknown action" << std::endl;
    return 1;
}
