/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "event.h"
#include "contact.h"
#include <vector>

void DisplayEvents(const sa::dav::DateTime& start, const sa::dav::DateTime& end, const std::vector<sa::dav::Event>& events, const std::string& itemTemplate, bool showCalendar, bool showEvents, bool markup);
void DisplayContacts(const std::vector<sa::dav::Contact>& contacts, const std::string& itemTemplate);
