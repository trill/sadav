/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <map>
#include <parseutils.h>
#include <type_traits>
#include <vector>
#include <client.h>

class ConfigFile
{
private:
    std::map<std::string, std::string> entires_;
public:
    ConfigFile();
    ~ConfigFile();
    bool Load(const std::string& filename);
    template<typename T>
    inline T Get(const std::string& name, T def) const;
    template<typename T>
    inline std::vector<T> GetList(const std::string& name) const;
};

template<>
inline std::string ConfigFile::Get<std::string>(const std::string& name, std::string def) const
{
    const auto it = entires_.find(name);
    if (it == entires_.end())
        return def;
    return (*it).second;
}

template<>
inline const char* ConfigFile::Get<const char*>(const std::string& name, const char* def) const
{
    const auto it = entires_.find(name);
    if (it == entires_.end())
        return def;
    return (*it).second.c_str();
}

template<>
inline bool ConfigFile::Get<bool>(const std::string& name, bool def) const
{
    const auto it = entires_.find(name);
    if (it == entires_.end())
        return def;
    std::string lowerValue = sa::dav::StringToLower<char>(it->second);
    return (lowerValue == "true" || lowerValue == "t" || lowerValue == "1" || lowerValue == "yes" || lowerValue == "y");
}

template<>
inline sa::dav::Client::Auth ConfigFile::Get<sa::dav::Client::Auth>(const std::string& name, sa::dav::Client::Auth def) const
{
    const auto it = entires_.find(name);
    if (it == entires_.end())
        return def;
    std::string lowerValue = sa::dav::StringToLower<char>(it->second);
    if (lowerValue == "basic")
        return sa::dav::Client::Auth::Basic;
    if (lowerValue == "digest")
        return sa::dav::Client::Auth::Digest;
    if (lowerValue == "try")
        return sa::dav::Client::Auth::Try;
    if (lowerValue == "none")
        return sa::dav::Client::Auth::None;
    return def;
}

template<typename T>
inline T ConfigFile::Get(const std::string& name, T def) const
{
    const auto it = entires_.find(name);
    if (it == entires_.end())
        return def;
    const auto res = sa::dav::ToNumber<T>((*it).second);
    if (res.has_value())
        return res.value();
    return def;
}

template<typename T>
inline std::vector<T> ConfigFile::GetList(const std::string& name) const
{
    std::vector<T> result;
    int index = 1;
    while (true)
    {
        std::string ident = name + "-" + std::to_string(index);
        auto it = entires_.find(ident);
        if (it == entires_.end())
            break;
        result.push_back(Get<T>(ident, {}));
        ++index;
    }
    return result;
}
