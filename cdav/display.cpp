/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "display.h"
#include <iomanip>
#include <iostream>
#include "template.h"
#include <parseutils.h>

static std::string GetArgument(const sa::templ::Token& token, const std::string& def)
{
    auto arg = sa::templ::GetArgument(token);
    if (!arg.empty())
        return arg;
    return def;
}

static std::string EventCallback(const sa::dav::Event& event, const sa::templ::Token& token)
{
    auto name = sa::dav::StringToLower(sa::templ::GetName(token));
    switch (token.type)
    {
    case sa::templ::Token::Type::Variable:
        if (name == "href")
            return event.GetHref();
        if (name == "etag")
            return event.GetEtag();
        if (name == "uid")
            return event.GetUID();
        if (name == "prodid")
            return event.prodid_;
        if (name == "version")
            return event.version_;
        if (name == "calendar")
            return event.GetOwner();
        if (name == "color")
            return event.GetColor().ToString();

        if (name == "today")
        {
            sa::dav::DateTime today = sa::dav::DateTime::Now();
            return event.IsOnDay(today) ? "->" : "";
        }
        if (name == "name" || name == "summary")
            return event.summary_;
        if (name == "allday")
            return event.IsAllDay() ? "*" : "";
        if (name == "created")
        {
            if (event.created_.Empty())
                return "";
            return event.created_.Format(GetArgument(token, "%d.%m.%Y %H:%M"));
        }
        if (name == "start")
        {
            if (event.start_.Empty())
                return "";
            return event.start_.Format(GetArgument(token, "%d.%m.%Y %H:%M"));
        }
        if (name == "end")
        {
            if (event.end_.Empty())
                return "";
            return event.end_.Format(GetArgument(token, "%d.%m.%Y %H:%M"));
        }
        if (name == "stamp")
        {
            if (event.stamp_.Empty())
                return "";
            return event.stamp_.Format(GetArgument(token, "%d.%m.%Y %H:%M"));
        }
        if (name == "modified")
        {
            if (event.modified_.Empty())
                return "";
            return event.modified_.Format(GetArgument(token, "%d.%m.%Y %H:%M"));
        }
        if (name == "due")
        {
            if (event.due_.Empty())
                return "";
            return event.due_.Format(GetArgument(token, "%d.%m.%Y %H:%M"));
        }
        if (auto it = event.other_.find(name); it != event.other_.end())
            return it->second;
        return "";
    default:
        return token.value;
    }
}

static bool DayHasEvents(const std::vector<sa::dav::Event>& events, const sa::dav::DateTime& day)
{
    for (const auto& e : events)
        if (e.IsOnDay(day))
            return true;
    return false;
}

static std::vector<std::string> GetMonthCal(const sa::dav::DateTime& dt, const std::vector<sa::dav::Event>& events, bool markup)
{
    std::vector<std::string> result;
    std::stringstream line;

    auto addLine = [&]()
    {
        result.push_back(line.str());
        line.str("");
    };

    constexpr int linelength = 28;

    tm target = *localtime(&dt.time_);
    target.tm_mday = 1;
    mktime(&target);
    auto heading = std::format("{} {}", dt.MonthName(), dt.Year());
    int align = (linelength / 2) - (heading.length() / 2);
    line << std::string(align, ' ');
    line << heading;
    line << std::string(linelength - align - heading.size(), ' ');
    addLine();
    line << "Sun Mon Tue Wed Thu Fri Sat ";
    addLine();
    int dow = target.tm_wday;
    line << std::string(4 * dow, ' ');
    // Markup implies no escape sequences
    bool noColor = markup || getenv("NO_COLOR");

    auto printDay = [&](int num, bool hasEvents, bool today)
    {
        if (hasEvents)
        {
            if (!noColor)
                line << "\e[4m";
            if (markup)
                line << "<u>";
        }
        if (target.tm_wday == 0 || target.tm_wday == 6)
        {
            if (!noColor)
                line << "\e[31m";
            if (markup)
                line << "<span color='red'>";
        }
        if (today)
        {
            if (!noColor)
                line << "\e[7m";
            if (markup)
            {
                if (target.tm_wday == 0 || target.tm_wday == 6)
                    line << "<span background='red' color='white'>";
                else
                    line << "<span background='white' color='black'>";
            }
        }
        line << std::setw(3) << std::setprecision(3) << num << " ";
        if (today)
        {
            if (markup)
                line << "</span>";
            if (!noColor)
                line << "\e[0m";
        }
        if (target.tm_wday == 0 || target.tm_wday == 6)
        {
            if (markup)
                line << "</span>";
            if (!noColor)
                line << "\e[0m";
        }
        if (hasEvents)
        {
            if (markup)
                line << "</u>";
            if (!noColor)
                line << "\e[0m";
        }
    };

    int month = target.tm_mon;
    sa::dav::DateTime today = sa::dav::DateTime::Now();
    for (int i = 1; target.tm_mon == month; target.tm_mday = ++i, mktime(&target))
    {
        sa::dav::DateTime current = sa::dav::DateTime(target);
        bool hasEvents = DayHasEvents(events, current.Date());
        bool isToday = current.SameDate(today);
        printDay(i, hasEvents, isToday);

        if (((i + dow) % 7) == 0)
            addLine();
    }
    return result;
}

static void PrintCalendar(const sa::dav::DateTime& dt, int months, const std::vector<sa::dav::Event>& events, bool markup)
{
    std::vector<std::string> calendar;
    for (int i = 0; i < months; ++i)
    {
        sa::dav::DateTime start = dt;
        start.Add(0, i, 0);
        auto month = GetMonthCal(start, events, markup);
        for (size_t i = 0; i < month.size(); ++i)
        {
            if (calendar.size() < i + 1)
                calendar.resize(i + 1);
            else
                calendar[i] += "  ";
            calendar[i] += month[i];
        }
    }
    for (const auto& line : calendar)
        std::cout << line << '\n';
}

void DisplayEvents(const sa::dav::DateTime& start, const sa::dav::DateTime& end, const std::vector<sa::dav::Event>& events, const std::string& itemTemplate, bool showCalendar, bool showEvents, bool markup)
{
    int months = (end.Month() - start.Month()) + 1;

    if (showCalendar)
    {
        PrintCalendar(start, months, events, markup);
        std::cout << std::endl;
    }

    if (showEvents)
    {
        std::string eventTemplate = itemTemplate;
        if (eventTemplate.empty())
            eventTemplate = "${today} ${name}: ${start(%d.%m.%Y)} ${start(%H:%M)}-${end(%H:%M)} ${allday}";
        for (const auto& event : events)
        {
            auto eventStr = sa::templ::Parser::Evaluate(eventTemplate, std::bind(&EventCallback, event, std::placeholders::_1));
            std::cout << eventStr << std::endl;
        }
    }
}

static std::string ContactCallback(const sa::dav::Contact& contact, const sa::templ::Token& token)
{
    auto name = sa::dav::StringToLower(sa::templ::GetName(token));
    switch (token.type)
    {
    case sa::templ::Token::Type::Variable:
        if (name == "href")
            return contact.GetHref();
        if (name == "etag")
            return contact.GetEtag();
        if (name == "uid")
            return contact.GetUID();
        if (name == "prodid")
            return contact.prodid_;
        if (name == "version")
            return contact.version_;
        if (name == "addressbook")
            return contact.GetOwner();

        if (name == "name")
            return contact.name_;
        if (name == "formatted_name")
            return contact.formattedName_;
        if (name == "bday")
        {
            if (contact.bday_.Empty())
                return "";
            return contact.bday_.Format(GetArgument(token, "%d.%m.%Y"));
        }
        if (name == "email")
        {
            auto type = GetArgument(token, "");
            if (contact.emails_.contains(type))
                return contact.emails_.at(type);
            if (!contact.emails_.empty() && type.empty())
                return contact.emails_.cbegin()->second;
            return "";
        }
        if (name == "tel")
        {
            auto type = GetArgument(token, "");
            if (contact.tels_.contains(type))
                return contact.tels_.at(type);
            if (!contact.tels_.empty() && type.empty())
                return contact.tels_.cbegin()->second;
            return "";
        }
        if (name == "address")
        {
            auto type = GetArgument(token, "");
            if (contact.addresses_.contains(type))
                return contact.addresses_.at(type);
            if (!contact.addresses_.empty() && type.empty())
                return contact.addresses_.cbegin()->second;
            return "";
        }
        if (auto it = contact.other_.find(name); it != contact.other_.end())
            return it->second;
        return "";
    default:
        return token.value;
    }
}

void DisplayContacts(const std::vector<sa::dav::Contact>& contacts, const std::string& itemTemplate)
{
    std::string contactTemplate = itemTemplate;
    if (contactTemplate.empty())
        contactTemplate = "${formatted_name} <${email}>";
    for (const auto& c : contacts)
        std::cout << sa::templ::Parser::Evaluate(contactTemplate, std::bind(&ContactCallback, c, std::placeholders::_1)) << std::endl;
}
