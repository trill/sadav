/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "object.h"
#include <sstream>
#include "config.h"
#include "datetime.h"
#include "parseutils.h"
#include "uuid.h"

namespace sa::dav {

ObjectType DetectObjectType(const std::string& contents)
{
    if (contents.empty())
        return ObjectType::Unknown;
    std::stringstream ss(contents);
    std::string line;
    while (std::getline(ss, line))
    {
        if (line.find("BEGIN:VCARD") != std::string::npos)
            return ObjectType::Contact;
        if (line.find("BEGIN:VEVENT") != std::string::npos)
            return ObjectType::Event;
        if (line.find("BEGIN:VTODO") != std::string::npos)
            return ObjectType::Todo;
        if (line.find("BEGIN:VJOURNAL") != std::string::npos)
            return ObjectType::Journal;
    }
    return ObjectType::Unknown;
}

bool ParseDateTime(const std::string& key, const std::string& value, DateTime& result)
{
    // Line: DTSTART;VALUE=DATE-TIME;TZID=Europe/Vienna:20210831T113000
    // Key: DTSTART;TZID=Europe/Vienna
    // Value: 20210831T113000
    Key parsedKey = ParseKey(key);
    if (parsedKey.values.empty())
        return result.Parse(value);
    std::string tz;
    auto tzIt = parsedKey.values.find("TZID");
    if (tzIt != parsedKey.values.end())
        tz = tzIt->second;
    auto valueIt = parsedKey.values.find("VALUE");
    DateTime::Value v = DateTime::Value::DateTime;
    if (valueIt != parsedKey.values.end())
    {
        if (valueIt->second == "DATE")
            v = DateTime::Value::Date;
    }

    return result.Parse(value, tz, v);
}

Object::Object(std::string href, std::string etag, std::string owner, std::string version) :
    prodid_(Config::Instance().Useragent()),
    version_(std::move(version)),
    uid_(RandomUUID()),
    href_(std::move(href)),
    etag_(std::move(etag)),
    owner_(std::move(owner))
{ }

Object::~Object() = default;

void Object::Clear()
{
    prodid_.clear();
    version_.clear();
    etag_.clear();
}

std::string Object::GetFilename() const
{
    auto pos = href_.find_last_of('/');
    if (pos == std::string::npos)
        return {};
    return href_.substr(pos + 1);
}

}
