/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <cstdint>
#include <random>
#include <sstream>
#include <iomanip>

namespace sa::dav {

std::string RandomUUID(bool decorate)
{
    uint8_t bytes[16] = {};
    static std::random_device dev;
    static std::mt19937 rng(dev());
    std::uniform_int_distribution<uint8_t> uid(0, 255);
    for (size_t i = 0; i < 16; ++i)
    {
        bytes[i] = uid(rng);
    }
    bytes[6] &= ~(1u << 7u);
    bytes[6] |= 1u << 6u;
    bytes[6] &= ~(1u << 5u);
    bytes[6] &= ~(1u << 4u);
    bytes[8] |= 1u << 7u;
    bytes[8] &= ~(1u << 6u);

    std::stringstream result;
    result << std::hex << std::setfill('0')
           << std::setw(2) << static_cast<int>(bytes[0])
           << std::setw(2) << static_cast<int>(bytes[1])
           << std::setw(2) << static_cast<int>(bytes[2])
           << std::setw(2) << static_cast<int>(bytes[3]);
    if (decorate)
        result << "-";
    result << std::setw(2) << static_cast<int>(bytes[4])
           << std::setw(2) << static_cast<int>(bytes[5]);
    if (decorate)
        result << "-";
    result << std::setw(2) << static_cast<int>(bytes[6])
           << std::setw(2) << static_cast<int>(bytes[7]);
    if (decorate)
        result << "-";
    result << std::setw(2) << static_cast<int>(bytes[8])
           << std::setw(2) << static_cast<int>(bytes[9]);
    if (decorate)
        result << "-";
    result << std::setw(2) << static_cast<int>(bytes[10])
           << std::setw(2) << static_cast<int>(bytes[11])
           << std::setw(2) << static_cast<int>(bytes[12])
           << std::setw(2) << static_cast<int>(bytes[13])
           << std::setw(2) << static_cast<int>(bytes[14])
           << std::setw(2) << static_cast<int>(bytes[15]);
    return result.str();
}

}
