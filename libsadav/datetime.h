/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <algorithm>
#include <chrono>
#include <string>
#include <string_view>

namespace sa::dav {

class DateTime
{
public:
    enum class Value
    {
        DateTime,
        Date
    };
    enum class Weekday
    {
        Sunday = 0,
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday
    };

    static DateTime Now();
    static DateTime WeekdayOfMonth(Weekday weekday, int nth, const DateTime& monthDate);

    DateTime();
    explicit DateTime(tm tm);

    bool Parse(const std::string& value, const std::string& tz = "", Value val = Value::DateTime);

    // Returns converted to tz_ or UTC
    std::string ToString(bool utc = false) const;
    // Returns local time
    std::string Format(const std::string& format) const;
    void Add(int y, int m, int d);
    void SetTime(const DateTime& value);
    void SetTime(int hour, int min, int sec);
    void SetDate(const DateTime& value);
    void SetDate(int year, int month, int mday);
    void AlignToWeekday(Weekday wd);
    // 0 - 6
    Weekday WeekDay() const;
    // 1 - 31
    int MonthDay() const;
    // 1 - 12
    int Month() const;
    // 19xx
    int Year() const;
    // Return how many days this month has
    int NumbertOfDays() const;
    // Return the closest next week day
    DateTime Next(Weekday weekday) const;
    DateTime Date() const;
    std::string_view MonthName() const;

    explicit operator tm() const;
    bool operator<(const DateTime& rhs) const;
    bool operator<=(const DateTime& rhs) const;
    bool operator>(const DateTime& rhs) const;
    bool operator>=(const DateTime& rhs) const;
    bool operator==(const DateTime& rhs) const;
    DateTime operator+(const DateTime& rhs) const;
    DateTime& operator+=(const DateTime& rhs);
    bool SameDate(const DateTime& other) const;
    bool Empty() const { return time_ == 0; }

    std::time_t time_{ 0 };
    std::string tz_;
    Value value_{ Value::DateTime };


    friend std::ostream& operator<<(std::ostream& os, const DateTime& value)
    {
        if (!value.tz_.empty())
            os << ";TZID=" << value.tz_;
        if (value.value_ != DateTime::Value::DateTime)
            os << ";VALUE=DATE";
        os << ":" << value.ToString();
        return os;
    }
};

}
