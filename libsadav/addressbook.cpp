/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "addressbook.h"
#include <pugixml.hpp>
#include <url.h>
#include <uuid.h>

namespace sa::dav {

AddressBook::AddressBook(std::string url, std::string username, std::string password) :
    Client(std::move(url), std::move(username), std::move(password))
{ }

bool AddressBook::Init()
{
    if (propfind_)
        return true;
    return Propfind();
}

bool AddressBook::Propfind()
{
    static constexpr std::string_view requestXML = R"xml(<?xml version="1.0" encoding="UTF-8"?>
<d:propfind xmlns:d="DAV:" xmlns:cs="http://calendarserver.org/ns/">
  <d:prop>
     <d:displayname />
     <cs:getctag />
  </d:prop>
</d:propfind>)xml";
    StringWriter writer;
    if (!Client::Propfind(std::string(requestXML), 0, writer))
        return false;

    if (!ParsePropfind(writer.contents_))
        return false;
    propfind_ = true;
    return true;
}

bool AddressBook::ParsePropfind(const std::string& body)
{
    pugi::xml_document doc;
    const pugi::xml_parse_result result = doc.load_string(body.c_str());
    if (result.status != pugi::status_ok)
    {
        Error("xml", result.description());
        return false;
    }

    auto prop = doc.first_element_by_path("d:multistatus/d:response/d:propstat/d:prop");
    if (!prop)
    {
        Error("dav", "multistatus/response/propstat/prop element not found");
        return false;
    }

    std::string displayName;
    std::string ctag;

    auto props = prop.children();
    for (const auto& prop : props)
    {
        std::string name = prop.name();
        if (name.ends_with("displayname"))
            displayName = prop.text().as_string();
        else if (name.ends_with("getctag"))
            ctag = prop.text().as_string();
    }

    auto stat = doc.first_element_by_path("d:multistatus/d:response/d:propstat/d:status");
    if (!stat)
    {
        Error("dav", "Status not found");
        return false;
    }
    std::string status = stat.text().as_string();
    if (!status.ends_with("200 OK"))
    {
        Error("dav", status);
        return false;
    }

    name_ = displayName;
    ctag_ = ctag;
    return true;
}

bool AddressBook::GetContacts(std::vector<Contact>& result)
{
    if (!Init())
        return false;

    static constexpr std::string_view requestXML = R"xml(<?xml version="1.0" encoding="UTF-8"?>
<card:addressbook-query xmlns:d="DAV:" xmlns:card="urn:ietf:params:xml:ns:carddav">
    <d:prop>
        <d:getetag />
        <card:address-data />
    </d:prop>
</card:addressbook-query>)xml";
    StringWriter writer;
    if (!MakeRequest(url_, "REPORT", 1, std::string(requestXML), "text/xml; charset=utf-8", &writer))
        return false;

    return ParseReport(writer.contents_, result);
}

bool AddressBook::ParseReport(const std::string& body, std::vector<Contact>& contacts)
{
    pugi::xml_document doc;
    const pugi::xml_parse_result result = doc.load_string(body.c_str());
    if (result.status != pugi::status_ok)
    {
        Error("xml", result.description());
        return false;
    }

    auto responses = doc.first_element_by_path("d:multistatus");
    if (!responses)
    {
        Error("dav", "multistatus element not found");
        return false;
    }

    for (const auto& response : responses.children("d:response"))
    {
        auto elStatus = response.first_element_by_path("d:propstat/d:status");
        if (!elStatus)
        {
            Error("dav", "No status element");
            return false;
        }
        if (!std::string(elStatus.text().as_string()).ends_with("200 OK"))
        {
            Error("dav", elStatus.text().as_string());
            return false;
        }

        std::string href;
        std::string etag;

        auto elHref = response.child("d:href");
        if (!elHref)
        {
            Error("dav", "No href found");
            return false;
        }
        href = elHref.text().as_string();

        auto elEtag = response.first_element_by_path("d:propstat/d:prop/d:getetag");
        if (elEtag)
            etag = elEtag.text().as_string();

        auto elCalDAta = response.first_element_by_path("d:propstat/d:prop/card:address-data");
        if (!elCalDAta)
        {
            Error("dav", "No address-data found");
            return false;
        }

        Contact contact(std::move(href), std::move(etag), name_);
        if (!contact.Parse(elCalDAta.text().as_string()))
        {
            Error("dav", "Invalid address-data");
            continue;
        }
        contacts.push_back(std::move(contact));
    }

    return true;
}

}
