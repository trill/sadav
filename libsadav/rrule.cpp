/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "rrule.h"
#include <cassert>
#include <iostream>
#include "event.h"
#include "parseutils.h"

namespace sa::dav {

RRule::RRule(std::string rule) : string_(std::move(rule))
{
    Parse(string_);
}

void RRule::GetRecurringDates(const Event& source,
    const DateTime& after,
    const DateTime& end,
    const DateTimeCallback& callback) const
{
    if (!valid_)
        return;

    switch (freq_)
    {
    case Frequency::Unknown:
    case Frequency::Secondly:
    case Frequency::Minutely:
    case Frequency::Hourly:
        // Skip those
        return;
    case Frequency::Daily:
        GetDailyDates(source, after, end, callback);
        break;
    case Frequency::Weekly:
        GetWeeklyDates(source, after, end, callback);
        break;
    case Frequency::Monthly:
        GetMonthlyDates(source, after, end, callback);
        break;
    case Frequency::Yearly:
        GetYearlyDates(source, after, end, callback);
        break;
    }
}

bool RRule::UnitlReached(const DateTime& test) const
{
    if (until_.Empty())
        return false;
    return test <= until_;
}

bool RRule::CountReached(int n) const
{
    if (count_ == -1)
        return false;
    return n > count_;
}

void RRule::GetYearlyDates(const Event& source,
    const DateTime& after,
    const DateTime& end,
    const DateTimeCallback& callback) const
{
    // TODO: BYMONTH
    // TODO: BYYEARDAY
    // TODO: BYWEEKNO

    DateTime testDate = source.start_;
    // Repeat every nth year since start
    // Start date is 1st occurrence
    int n = 1;
    while (testDate <= end)
    {
        if (!source.IsExcluded(testDate))
        {
            ++n;
            if (CountReached(n))
                return;
            if (UnitlReached(testDate))
                return;
            if (testDate > end)
                return;
            if (testDate > after)
                callback(testDate);
        }
        testDate.Add(interval_, 0, 0);
    }
}

void RRule::GetMonthlyDatesBydays(const Event& source,
    const DateTime& after,
    const DateTime& end,
    const DateTimeCallback& callback) const
{
    // Repeat every nth week at given month day
    DateTime testDate = source.start_;
    for (const auto& byday : bydays_)
    {
        if (byday.nth == 0)
        {
            // No selector -> every nth weeksday in of the month
            testDate.AlignToWeekday(byday.weekday);

            int n = 0;
            while (testDate <= end)
            {
                if (!source.IsExcluded(testDate))
                {
                    ++n;
                    if (CountReached(n))
                        return;
                    if (UnitlReached(testDate))
                        return;
                    if (testDate > end)
                        return;
                    if (testDate > after)
                        callback(testDate);
                }

                int month = testDate.Month();
                int year = testDate.Year();
                testDate.Add(0, 0, 7);
                if (month != testDate.Month())
                {
                    testDate.SetDate(year, month, 1);
                    testDate.Add(0, interval_, 0);
                    testDate.AlignToWeekday(byday.weekday);
                }
            }
        }
        else
        {
            // Selector is set -> specific weekdays of the month
            int n = 0;
            while (testDate <= end)
            {
                DateTime weekdayDate = DateTime::WeekdayOfMonth(byday.weekday, byday.nth, testDate);

                if (!source.IsExcluded(weekdayDate))
                {
                    ++n;
                    if (CountReached(n))
                        return;
                    if (UnitlReached(weekdayDate))
                        return;
                    if (weekdayDate > end)
                        return;
                    if (weekdayDate > after)
                        callback(weekdayDate);
                }
                testDate.SetDate(weekdayDate);
                testDate.Add(0, interval_, 0);
                testDate.SetDate(testDate.Year(), testDate.Month(), 1);
                testDate.AlignToWeekday(byday.weekday);
            }
        }
    }
}

void RRule::GetMonthlyDatesBymonthdays(const Event& source,
    const DateTime& after,
    const DateTime& end,
    const DateTimeCallback& callback) const
{
    // Monthly on the 2nd and 15th of the month for 10 occurrences:
    // RRULE:FREQ=MONTHLY;COUNT=10;BYMONTHDAY=2,15

    // Negative
    // Monthly on the third-to-the-last day of the month, forever:
    // RRULE:FREQ=MONTHLY;BYMONTHDAY=-3
    DateTime testDate = source.start_;
    int n = 0;
    while (testDate <= end)
    {
        for (int day : bymonthdays_)
        {
            if (day < 0)
                day = testDate.NumbertOfDays() - day;
            if (day <= testDate.NumbertOfDays())
            {
                testDate.SetDate(testDate.Year(), testDate.Month(), day);
                if (!source.IsExcluded(testDate))
                {
                    ++n;
                    if (CountReached(n))
                        return;
                    if (UnitlReached(testDate))
                        return;
                    if (testDate > end)
                        return;
                    if (testDate > after)
                        callback(testDate);
                }
            }
        }
        testDate.Add(0, interval_, 0);
    }
}

void RRule::GetMonthlyDates(const Event& source,
    const DateTime& after,
    const DateTime& end,
    const DateTimeCallback& callback) const
{
    if (!bymonthdays_.empty())
        return GetMonthlyDatesBymonthdays(source, after, end, callback);
    // TODO: BYWEEKNO
    if (!bydays_.empty())
        return GetMonthlyDatesBydays(source, after, end, callback);

    // No bydays
    DateTime testDate = source.start_;

    // Repeat every nth month since start
    int n = 0;
    while (testDate <= end)
    {
        if (!source.IsExcluded(testDate))
        {
            ++n;
            if (CountReached(n))
                return;
            if (UnitlReached(testDate))
                return;
            if (testDate > end)
                return;
            if (testDate > after)
                callback(testDate);
        }
        testDate.Add(0, interval_, 0);
    }
}

void RRule::GetWeeklyDates(const Event& source,
    const DateTime& after,
    const DateTime& end,
    const DateTimeCallback& callback) const
{
    DateTime testDate = source.start_;
    if (bydays_.empty())
    {
        // Repeat every nth week since start
        int n = 0;
        while (testDate <= end)
        {
            if (!source.IsExcluded(testDate))
            {
                ++n;
                if (CountReached(n))
                    return;
                if (UnitlReached(testDate))
                    return;
                if (testDate > end)
                    return;
                if (testDate > after)
                    callback(testDate);
            }
            testDate.Add(0, 0, 7 * interval_);
        }
        return;
    }

    // Repeat every nth week at given week day
    for (const auto& byday : bydays_)
    {
        testDate.AlignToWeekday(byday.weekday);

        int n = 0;
        while (testDate <= end)
        {
            if (!source.IsExcluded(testDate))
            {
                ++n;
                if (CountReached(n))
                    return;
                if (UnitlReached(testDate))
                    return;
                if (testDate > end)
                    return;
                if (testDate > after)
                    callback(testDate);
            }
            testDate.Add(0, 0, 7 * interval_);
        }
    }
}

void RRule::GetDailyDates(const Event& source,
    const DateTime& after,
    const DateTime& end,
    const DateTimeCallback& callback) const
{
    DateTime testDate = source.start_;
    // Repeat every nth day since start
    int n = 0;
    while (testDate <= end)
    {
        if (!source.IsExcluded(testDate))
        {
            ++n;
            if (CountReached(n))
                return;
            if (UnitlReached(testDate))
                return;
            if (testDate > end)
                return;
            if (testDate > after)
                callback(testDate);
        }
        testDate.Add(0, 0, interval_);
    }
}

static DateTime::Weekday WeekdayIndex(const std::string& value)
{
    if (value.ends_with("SO"))
        return DateTime::Weekday::Sunday;
    if (value.ends_with("MO"))
        return DateTime::Weekday::Monday;
    if (value.ends_with("TU"))
        return DateTime::Weekday::Tuesday;
    if (value.ends_with("WE"))
        return DateTime::Weekday::Wednesday;
    if (value.ends_with("TH"))
        return DateTime::Weekday::Thursday;
    if (value.ends_with("FR"))
        return DateTime::Weekday::Friday;
    if (value.ends_with("SA"))
        return DateTime::Weekday::Saturday;
    return (DateTime::Weekday)-1;
}

static int WeekdaySelector(const std::string& value)
{
    // weekdaynum  = [[plus / minus] ordwk] weekday
    if (value.length() < 3)
        return -1;
    std::string num = value.substr(0, value.length() - 2);
    auto r = ToNumber<int>(num);
    if (r.has_value())
        return *r;
    return -1;
}

static void ParseNumberList(const std::string& value, std::vector<int>& result, bool allowZero = true)
{
    auto parts = Split(value, ",", false, false);
    for (const auto& p : parts)
    {
        auto n = ToNumber<int>(p);
        if (n.has_value())
        {
            if (allowZero || *n != 0)
                result.push_back(*n);
        }
    }
}

void RRule::ParseBydayList(const std::string& value)
{
    // bywdaylist  = ( weekdaynum *("," weekdaynum) )
    // weekdaynum  = [[plus / minus] ordwk] weekday
    auto parts = Split(value, ",", false, false);
    for (const auto& p : parts)
    {
        int wd = WeekdaySelector(p);
        DateTime::Weekday index = WeekdayIndex(p);
        if ((int)index != -1)
            bydays_.push_back({ wd, index });
    }
}

void RRule::Parse(const std::string& value)
{
    auto rules = Split(value, ";", false, false);

    for (const auto& rule : rules)
    {
        auto keyvalue = Split(rule, "=", false, false);
        if (keyvalue.size() != 2)
            continue;

        if (keyvalue[0] == "FREQ")
        {
            if (keyvalue[1] == "SECONDLY")
                freq_ = Frequency::Secondly;
            else if (keyvalue[1] == "MINUTELY")
                freq_ = Frequency::Minutely;
            else if (keyvalue[1] == "HOURLY")
                freq_ = Frequency::Hourly;
            else if (keyvalue[1] == "DAILY")
                freq_ = Frequency::Daily;
            else if (keyvalue[1] == "WEEKLY")
                freq_ = Frequency::Weekly;
            else if (keyvalue[1] == "MONTHLY")
                freq_ = Frequency::Monthly;
            else if (keyvalue[1] == "YEARLY")
                freq_ = Frequency::Yearly;
            else
                // Need a valid frequency
                return;
        }
        else if (keyvalue[0] == "INTERVAL")
        {
            auto i = ToNumber<int>(keyvalue[1]);
            if (!i.has_value())
                return;
            interval_ = *i;
        }
        else if (keyvalue[0] == "COUNT")
        {
            auto i = ToNumber<int>(keyvalue[1]);
            if (!i.has_value())
                return;
            count_ = *i;
        }
        else if (keyvalue[0] == "UNTIL")
        {
            if (!until_.Parse(keyvalue[1]))
                return;
        }
        else if (keyvalue[0] == "WKST")
        {
            wkst_ = WeekdayIndex(keyvalue[1]);
            if ((int)wkst_ == -1)
                return;
        }
        else if (keyvalue[0] == "BYDAY")
        {
            ParseBydayList(keyvalue[1]);
        }
        else if (keyvalue[0] == "BYMONTHDAY")
        {
            ParseNumberList(keyvalue[1], bymonthdays_, false);
        }
        else if (keyvalue[0] == "BYMONTH")
        {
            ParseNumberList(keyvalue[1], bymonts_, false);
        }
        else if (keyvalue[0] == "BYYEARDAY")
        {
            ParseNumberList(keyvalue[1], byyeardays_);
        }
        else if (keyvalue[0] == "BYWEEKNO")
        {
            ParseNumberList(keyvalue[1], byweeks_);
        }
    }

    if (freq_ == Frequency::Unknown)
        return;

    valid_ = true;
}

}
