/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <strings.h>
#include <map>
#include <string>

namespace sa::dav {

namespace detail {

struct CIComparator
{
    bool operator()(const std::string& a, const std::string& b) const noexcept
    {
        return ::strcasecmp(a.c_str(), b.c_str()) < 0;
    }
};

}

template <typename T>
using CaseInsensitiveMap = std::map<std::string, T, detail::CIComparator>;

}
