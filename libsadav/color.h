/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <cstdint>
#include <string>

namespace sa::dav {

struct Color
{
    static Color FromString(const std::string& value);
    static Color Random();
    std::string ToString() const;

    uint8_t r = 0;
    uint8_t g = 0;
    uint8_t b = 0;
};

}
