/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>

namespace sa::dav {

class Config
{
public:
    static Config& Instance();

    const std::string& Useragent() const { return useragent_; }
    const std::string& Timezone() const { return timezone_; }

    void SetUseragent(std::string value);
    void SetTimezone(std::string value);
private:
    Config();

    std::string useragent_;
    std::string timezone_;
};

}
