/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "alarm.h"
#include "event.h"

namespace sa::dav {

void Alarm::Parse(std::stringstream& ss)
{
    std::string line;
    while (NextField(ss, line))
    {
        if (line.find("END:VALARM") != std::string::npos)
            break;

        auto colonPos = line.find(':');
        if (colonPos == std::string::npos)
            continue;
        std::string key = line.substr(0, colonPos);
        std::string value = line.substr(colonPos + 1);
        Key parsedKey = ParseKey(key);

        if (parsedKey.name == "ACTION")
        {
            if (value == "DISPLAY")
                action_ = Alarm::Action::Display;
            if (value == "AUDIO")
                action_ = Alarm::Action::Audio;
            if (value == "EMAIL")
                action_ = Alarm::Action::Email;
            if (value == "PROCEDURE")
                action_ = Alarm::Action::Procedure;
        }
        else if (parsedKey.name == "REPEAT")
        {
            repeat_ = ToNumber<int>(value).value_or(0);
        }
        else if (parsedKey.name == "DURATION")
        {
            duration_ = ParseDuration(value).value_or(0);
        }
        fields_.emplace(key, value);
    }
}

void Alarm::Clear()
{
    action_ = Action::None;
    repeat_ = 0;
    duration_ = 0 ;
    fields_.clear();
}

DateTime Alarm::GetRelevant(const Key& key, const Event& event)
{
    auto relit = key.values.find("RELATED");
    if (relit != key.values.end())
    {
        if (relit->second == "END")
        {
            // after the end of an event or the due date of a to-do.
            if (event.GetType() == Event::Type::Todo)
                return event.due_;
            return event.end_;
        }
    }
    return event.start_;
}

std::optional<Alarm::Trigger> Alarm::GetTrigger(const Event& event) const
{
    auto parse = [&event](const Key& key, const std::string& value) -> Trigger
    {
        auto rel = GetRelevant(key, event);
        if (auto it = key.values.find("VALUE"); it != key.values.end())
        {
            // TRIGGER;VALUE=DATE-TIME:19970317T133000Z
            if (it->second != "DATE-TIME")
                return {};
            DateTime result;
            if (result.Parse(value))
                return { .trigger = result, .relevant = rel };
            return {};
        }

        // Relative
        auto dur = ParseDuration(value);
        if (!dur.has_value())
            return {};
        DateTime result = rel;
        result.time_ += *dur;
        return { .trigger = result, .relevant = rel };
    };

    for (const auto& f : fields_)
    {
        Key k = ParseKey(f.first);
        if (k.name == "TRIGGER")
        {
            auto res = parse(k, f.second);
            if (!res.trigger.Empty() && !res.relevant.Empty())
                return res;
        }
    }
    return {};
}

std::string Alarm::GetSummary(const Event& event) const
{
    auto it = fields_.find("SUMMARY");
    if (it != fields_.end())
        return ProcessString(it->second);
    return event.summary_;
}

std::string Alarm::GetDescription(const Event& event) const
{
    if (auto it = fields_.find("DESCRIPTION"); it != fields_.end())
        return ProcessString(it->second);
    if (auto it = event.other_.find("DESCRIPTION"); it != event.other_.end())
        return ProcessString(it->second);
    return "";
}


}
