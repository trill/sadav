/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "url.h"
#include <algorithm>
#include <sstream>
#include <iomanip>

namespace sa::dav {

std::string URLEncode(const std::string& url)
{
    std::stringstream ss;
    ss.fill('0');
    ss << std::hex;
    for (const auto& c : url)
    {
        if (std::isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~')
        {
            ss << c;
            continue;
        }

        ss << std::uppercase;
        ss << '%' << std::setw(2) << (int)(c);
        ss << std::nouppercase;
    }
    return ss.str();
}

std::string URLDecode(const std::string& url)
{
    std::stringstream ss;
    for (std::string::const_iterator i = url.begin(), n = url.end(); i != n; ++i)
    {
        std::string::value_type c = (*i);
        if (c == '%')
        {
            char hex[3] = {};
            ++i;
            hex[0] = (*i);
            ++i;
            hex[1] = (*i);
            uint32_t value = std::stoi(hex, nullptr, 16);
            ss <<  (char)value;
        }
        else if (c == '+')
            ss << ' ';
        else
            ss << c;
    }
    return ss.str();
}

URL::URL(const std::string& url)
{
    Parse(url);
}

void URL::Parse(const std::string& url)
{
    const std::string scheme_end("://");
    auto scheme_i = std::search(url.begin(), url.end(),
        scheme_end.begin(), scheme_end.end());
    scheme_.reserve(distance(url.begin(), scheme_i));
    std::transform(url.begin(), scheme_i,
        std::back_inserter(scheme_),
        ::tolower);
    if (scheme_i == url.end() )
        return;
    std::advance(scheme_i, scheme_end.length());

    auto path_i = std::find(scheme_i, url.end(), '/');
    host_.reserve(std::distance(scheme_i, path_i));
    std::transform(scheme_i, path_i,
        std::back_inserter(host_),
        ::tolower);
    auto ppos = host_.find(':');
    if (ppos != std::string::npos)
    {
        auto port = host_.substr(ppos + 1);
        host_.erase(ppos);
        port_ = std::atoi(port.c_str());
    }
    auto query_i = find(path_i, url.end(), '?');
    path_.assign(path_i, query_i);
    if (path_.empty())
        path_ = std::string("/");
    if( query_i != url.end() )
        ++query_i;
    query_.assign(query_i, url.end());
}

bool URL::IsDefaultPort() const
{
    if (port_ == 0)
        return false;
    if (scheme_ == "http")
        return port_ == 80;
    if (scheme_ == "https")
        return port_ == 443;
    return false;
}

uint16_t URL::Port() const
{
    if (port_ == 0)
    {
        if (scheme_ == "http")
            return 80;
        if (scheme_ == "https")
            return 443;
        return 0;
    }
    return port_;
}

std::string URL::SchemeHostPort() const
{
    std::stringstream ss;
    ss << scheme_ + "://" << host_;
    if (!IsDefaultPort())
        ss << ":" << Port();
    return ss.str();
}

std::string URL::PathQuery() const
{
    std::string result = path_;
    if (!query_.empty())
        result += "?" + query_;
    return result;
}

}
