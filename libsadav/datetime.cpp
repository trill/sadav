/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "datetime.h"
#include <date/date.h>
#include <date/tz.h>
#include <array>
#include <chrono>
#include <iostream>
#include "config.h"

namespace sa::dav {

static int GetNumberOfDays(int month, int year)
{
    if (month == 2)
    {
        if ((year % 400 == 0) || (year % 4 == 0 && year % 100 != 0))
            return 29;
        return 28;
    }
    if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
        return 31;
    return 30;
}

static bool TryParseFormat(const std::string& value, const std::string& format, time_t& out, const std::string& tz)
{
    std::istringstream in{ value };
    if (tz.empty())
    {
        date::sys_seconds tp;
        in >> date::parse(format, tp);
        if (!in.fail())
        {
            out = tp.time_since_epoch().count();
            return true;
        }
        return false;
    }

    date::local_seconds tp;
    in >> date::parse(format, tp);
    if (!in.fail())
    {
        auto zoned = date::make_zoned(tz, tp);
        out = zoned.get_sys_time().time_since_epoch().count();
        return true;
    }
    return false;
}

DateTime DateTime::Now()
{
    using namespace std::chrono;
    DateTime result;
    result.time_ = system_clock::to_time_t(floor<seconds>(system_clock::now()));
    return result;
}

DateTime DateTime::WeekdayOfMonth(Weekday weekday, int nth, const DateTime& monthDate)
{
    DateTime result = monthDate;
    result.SetDate(result.Year(), result.Month(), 1);
    result = result.Next(weekday);
    result.Add(0, 0, (nth - 1) * 7);
    return result;
}

DateTime::DateTime() :
    tz_(Config::Instance().Timezone())
{
}

DateTime::DateTime(tm tm) :
    time_(mktime(&tm))
{
    // Accepts out-of-range struct member values and as a side-product of conversion set them (and all others)
    // appropriately.
}

DateTime DateTime::Next(Weekday weekday) const
{
    DateTime result = *this;
    result.Add(0, 0, (weekday < result.WeekDay() ? 7 : 0) + static_cast<int>(weekday) - static_cast<int>(result.WeekDay()));
    return result;
}

DateTime DateTime::Date() const
{
    DateTime result = *this;
    result.SetTime(0, 0, 0);
    return result;
}

void DateTime::Add(int y, int m, int d)
{
    auto tm = *localtime(&time_);
    tm.tm_year += y;
    tm.tm_mon += m;
    tm.tm_mday += d;
    time_ = mktime(&tm);
}

void DateTime::SetTime(const DateTime& value)
{
    auto thisTm = (tm)*this;
    auto otherTm = (tm)value;
    thisTm.tm_hour = otherTm.tm_hour;
    thisTm.tm_min = otherTm.tm_min;
    thisTm.tm_sec = otherTm.tm_sec;
    time_ = mktime(&thisTm);
}

void DateTime::SetTime(int hour, int min, int sec)
{
    auto thisTm = (tm)*this;
    thisTm.tm_hour = hour;
    thisTm.tm_min = min;
    thisTm.tm_sec = sec;
    time_ = mktime(&thisTm);
}

void DateTime::SetDate(const DateTime& value)
{
    auto thisTm = (tm)*this;
    auto otherTm = (tm)value;
    thisTm.tm_year = otherTm.tm_year;
    thisTm.tm_mon = otherTm.tm_mon;
    thisTm.tm_mday = otherTm.tm_mday;
    thisTm.tm_yday = otherTm.tm_yday;
    // We don't know if it is DST
    thisTm.tm_isdst = -1;
    time_ = mktime(&thisTm);
}

void DateTime::SetDate(int year, int month, int mday)
{
    auto thisTm = (tm)*this;
    thisTm.tm_year = year - 1900;
    thisTm.tm_mon = month - 1;
    thisTm.tm_mday = mday;
    thisTm.tm_isdst = -1;
    time_ = mktime(&thisTm);
}

void DateTime::AlignToWeekday(Weekday wd)
{
    int diff = (int)wd - (int)WeekDay();
    if (diff == 0)
        return;
    if (diff < 0)
        diff += 7;
    Add(0, 0, diff);
}

bool DateTime::Parse(const std::string& value, const std::string& tz, Value val)
{
    value_ = val;
    // https://www.rfc-editor.org/rfc/rfc4791#section-7.3
    if (TryParseFormat(value, "%Y%m%dT%H%M%SZ", time_, ""))
    {
        // No timezone its UTC
        tz_ = "";
        return true;
    }
    if (TryParseFormat(value, "%Y%m%dT%H%M%S", time_, tz))
    {
        tz_ = tz;
        return true;
    }
    if (TryParseFormat(value, "%Y%m%d%H%M%S", time_, tz))
    {
        tz_ = tz;
        return true;
    }
    if (TryParseFormat(value, "%Y%m%dT%H%M", time_, tz))
    {
        tz_ = tz;
        return true;
    }
    if (TryParseFormat(value, "%Y%m%d%H%M", time_, tz))
    {
        tz_ = tz;
        return true;
    }
    if (TryParseFormat(value, "%Y%m%d", time_, tz))
    {
        tz_ = tz;
        return true;
    }
    if (TryParseFormat(value, "%Y-%m-%d", time_, tz))
    {
        tz_ = tz;
        return true;
    }

    return false;
}

std::string DateTime::ToString(bool utc) const
{
    if (tz_.empty() || tz_ == "UTC" || utc)
    {
        struct tm* tm = gmtime(&time_);
        std::array<char, 256> buff;
        if (value_ == Value::DateTime)
            strftime(buff.data(), sizeof(buff), "%Y%m%dT%H%M%SZ", tm);
        else
            strftime(buff.data(), sizeof(buff), "%Y%m%d", tm);
        return buff.data();
    }

    auto zoned = date::make_zoned(tz_, std::chrono::floor<std::chrono::seconds>(std::chrono::system_clock::from_time_t(time_)));
    if (value_ == Value::DateTime)
        return date::format("%Y%m%dT%H%M%S", zoned);
    return date::format("%Y%m%d", zoned);
}

std::string DateTime::Format(const std::string& format) const
{
    struct tm* tm = localtime(&time_);
    std::array<char, 256> buff;
    strftime(buff.data(), sizeof(buff), format.c_str(), tm);
    return buff.data();
}

DateTime::operator tm() const
{
    return *localtime(&time_);
}

bool DateTime::operator<(const DateTime& rhs) const
{
    return time_ < rhs.time_;
}

bool DateTime::operator<=(const DateTime& rhs) const
{
    return (*this < rhs) || (*this == rhs);
}

bool DateTime::operator>(const DateTime& rhs) const
{
    return time_ > rhs.time_;
}

bool DateTime::operator>=(const DateTime& rhs) const
{
    return (*this > rhs) || (*this == rhs);
}

bool DateTime::operator==(const DateTime& rhs) const
{
    return time_ == rhs.time_;
}

DateTime DateTime::operator+(const DateTime& rhs) const
{
    DateTime result;
    result.time_ = time_ + rhs.time_;
    return result;
}

DateTime& DateTime::operator+=(const DateTime& rhs)
{
    time_ += rhs.time_;
    return *this;
}

bool DateTime::SameDate(const DateTime& other) const
{
    struct tm us = *localtime(&time_);
    struct tm them = *localtime(&other.time_);
    return us.tm_year == them.tm_year && us.tm_mon == them.tm_mon && us.tm_mday == them.tm_mday;
}

DateTime::Weekday DateTime::WeekDay() const
{
    struct tm* us = localtime(&time_);
    return (Weekday)us->tm_wday;
}

int DateTime::MonthDay() const
{
    struct tm* us = localtime(&time_);
    return us->tm_mday;
}

int DateTime::Month() const
{
    struct tm* us = localtime(&time_);
    return us->tm_mon + 1;
}

int DateTime::Year() const
{
    struct tm* us = localtime(&time_);
    return us->tm_year + 1900;
}

int DateTime::NumbertOfDays() const
{
    return GetNumberOfDays(Month(), Year());
}

std::string_view DateTime::MonthName() const
{
    switch (Month())
    {
    case 1:
        return "January";
    case 2:
        return "February";
    case 3:
        return "March";
    case 4:
        return "April";
    case 5:
        return "May";
    case 6:
        return "June";
    case 7:
        return "July";
    case 8:
        return "August";
    case 9:
        return "September";
    case 10:
        return "October";
    case 11:
        return "November";
    case 12:
        return "December";
    default:
        return "Unknown";
    }
}

}
