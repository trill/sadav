/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <vector>
#include <string>
#include <optional>
#include <sstream>
#include "cimap.h"

namespace sa::dav {

inline constexpr int SECONDS_PER_WEEK = 604800;
inline constexpr int SECONDS_PER_DAY = 86400;
inline constexpr int SECONDS_PER_HOUR = 3600;
inline constexpr int SECONDS_PER_MINUTE = 60;

struct Key
{
    std::string name;
    CaseInsensitiveMap<std::string> values;
};

Key ParseKey(const std::string& key);
bool NextField(std::stringstream& ss, std::string& result);
// Returns seconds
std::optional<int> ParseDuration(const std::string& value);

bool IsWhite(const std::string& s);
std::vector<std::string> Split(const std::string& str,
    const std::string& delim,
    bool keepEmpty = false,
    bool keepWhite = true);
template<typename T>
inline std::optional<T> ToNumber(std::string_view number)
{
    T result;
    std::string snumber(number);
    std::stringstream iss;
    if (snumber.starts_with("0x") || snumber.starts_with("0X"))
        iss << std::hex;
    iss << snumber;
    iss >> result;
    if (iss.fail())
        return {};
    return result;
}

template <typename charType>
inline std::basic_string<charType> Trim(const std::basic_string<charType>& str,
    const std::basic_string<charType>& whitespace = " \t")
{
    // Left
    const auto strBegin = str.find_first_not_of(whitespace);
    if (strBegin == std::string::npos)
        return std::basic_string<charType>(); // no content

    // Right
    const auto strEnd = str.find_last_not_of(whitespace);
    const auto strRange = strEnd - strBegin + 1;

    return str.substr(strBegin, strRange);
}
template <typename charType>
inline std::basic_string<charType> LeftTrim(const std::basic_string<charType>& str,
    const std::basic_string<charType>& whitespace = " \t")
{
    const auto strBegin = str.find_first_not_of(whitespace);
    if (strBegin == std::basic_string<charType>::npos)
        return ""; // no content

    return str.substr(strBegin, std::basic_string<charType>::npos);
}

template <typename charType>
inline std::basic_string<charType> RightTrim(const std::basic_string<charType>& str,
    const std::basic_string<charType>& whitespace = " \t")
{
    const auto strEnd = str.find_last_not_of(whitespace);
    if (strEnd == std::basic_string<charType>::npos)
        return ""; // no content

    return str.substr(0, strEnd + 1);
}
template <typename charType>
inline charType ToLower(charType)
{
}
template <>
inline char ToLower<char>(char c)
{
    return static_cast<char>(std::tolower(c));
}
template <>
inline wchar_t ToLower<wchar_t>(wchar_t c)
{
    return std::towlower(c);
}
template <typename charType>
inline charType ToUpper(charType)
{
}
template <>
inline char ToUpper<char>(char c)
{
    return static_cast<char>(std::toupper(c));
}
template <>
inline wchar_t ToUpper<wchar_t>(wchar_t c)
{
    return std::towupper(c);
}

template <typename charType>
inline std::basic_string<charType> StringToUpper(const std::basic_string<charType>& s)
{
    std::basic_string<charType> result;
    result.resize(s.length());
    for (size_t i = 0; i < s.length(); ++i)
        result[i] = ToUpper<charType>(s[i]);
    return result;
}
template <typename charType>
inline std::basic_string<charType> StringToLower(const std::basic_string<charType>& s)
{
    std::basic_string<charType> result;
    result.resize(s.length());
    for (size_t i = 0; i < s.length(); ++i)
        result[i] = ToLower<charType>(s[i]);
    return result;
}

/// Replace all occurrences of search with replace in subject
/// Returns true if it replaced something
template <typename charType>
inline bool ReplaceSubstring(std::basic_string<charType>& subject,
    const std::basic_string<charType>& search,
    const std::basic_string<charType>& replace)
{
    if (search.empty())
        return false;
    bool result = false;

    using string_type = std::basic_string<charType>;
    string_type newString;
    newString.reserve(subject.length());

    size_t lastPos = 0;
    size_t findPos;

    while ((findPos = subject.find(search, lastPos)) != string_type::npos)
    {
        newString.append(subject, lastPos, findPos - lastPos);
        newString += replace;
        lastPos = findPos + search.length();
        result = true;
    }

    newString += subject.substr(lastPos);

    subject.swap(newString);
    return result;
}

std::string ProcessString(const std::string& value);

}
