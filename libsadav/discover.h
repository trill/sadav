/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "client.h"
#include <string>
#include <vector>

namespace sa::dav {

class Discover final : public Client
{
public:
    Discover(std::string url, std::string username, std::string password);
    std::string GetName() const override { return "Discover"; }

    std::string UserPrincipal();
    std::vector<std::string> GetFeatures();
    std::vector<std::pair<std::string, std::string>> List();
};

}
