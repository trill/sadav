/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "datetime.h"
#include <string>
#include <vector>
#include <functional>

namespace sa::dav {

class Event;

using DateTimeCallback = std::function<void(const DateTime&)>;

// https://www.rfc-editor.org/rfc/rfc5545
// https://icalendar.org/iCalendar-RFC-5545/3-8-5-3-recurrence-rule.html
class RRule
{
public:
    enum class Frequency
    {
        Unknown,
        Secondly,
        Minutely,
        Hourly,
        Daily,
        Weekly,
        Monthly,
        Yearly
    };

    RRule() = default;
    explicit RRule(std::string rule);
    void GetRecurringDates(const Event& source, const DateTime& after, const DateTime& end, const DateTimeCallback& callback) const;

    bool IsValid() const { return valid_; }
    const std::string& AsString() const { return string_; }
private:
    struct Byday
    {
        // Every nth week day
        int nth = 0;
        // -1 = not set, 0 = SU
        DateTime::Weekday weekday = (DateTime::Weekday)-1;
    };
    void Parse(const std::string& value);
    void ParseBydayList(const std::string& value);
    void GetYearlyDates(const Event& source, const DateTime& after, const DateTime& end, const DateTimeCallback& callback) const;
    void GetMonthlyDates(const Event& source, const DateTime& after, const DateTime& end, const DateTimeCallback& callback) const;
    void GetMonthlyDatesBydays(const Event& source, const DateTime& after, const DateTime& end, const DateTimeCallback& callback) const;
    void GetMonthlyDatesBymonthdays(const Event& source, const DateTime& after, const DateTime& end, const DateTimeCallback& callback) const;
    void GetWeeklyDates(const Event& source, const DateTime& after, const DateTime& end, const DateTimeCallback& callback) const;
    void GetDailyDates(const Event& source, const DateTime& after, const DateTime& end, const DateTimeCallback& callback) const;
    bool UnitlReached(const DateTime& test) const;
    bool CountReached(int n) const;
    bool valid_{ false };
    Frequency freq_{ Frequency::Unknown };
    DateTime until_;
    int interval_{ 1 };
    // Weekday: "SU" / "MO" / "TU" / "WE" / "TH" / "FR" / "SA"
    DateTime::Weekday wkst_{ (DateTime::Weekday)-1 };
    int count_{ -1 };
    std::vector<Byday> bydays_;
    std::vector<int> bymonthdays_;
    std::vector<int> bymonts_;
    std::vector<int> byyeardays_;
    std::vector<int> byweeks_;
    std::vector<int> bysetpos_;

    std::string string_;
};

}
