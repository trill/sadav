/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <functional>
#include <map>
#include <memory>
#include <string>
#include "cimap.h"
#include "object.h"
#include <fstream>
#include "color.h"
#include <curl/curl.h>

namespace sa::dav {

class Writer
{
public:
    virtual ~Writer();
    virtual void Write(char*, size_t) = 0;
};

class StringWriter : public Writer
{
public:
    void Write(char*, size_t) override;
    std::string contents_;
};

class FileWriter : public Writer
{
public:
    FileWriter(const std::string& filename);
    void Write(char*, size_t) override;
private:
    std::ofstream stream_;
};

class Reader
{
public:
    virtual ~Reader();
    virtual size_t Read(char* buff, size_t size) = 0;
    virtual size_t Size() const = 0;
    std::string contentType_;
};

class StringReader : public Reader
{
public:
    StringReader(std::string content);
    StringReader(const char* content);
    size_t Read(char* buff, size_t size) override;
    size_t Size() const override;
private:
    size_t pos_{ 0 };
    std::string content_;
};

class FileReader : public Reader
{
public:
    FileReader(const std::string& filename);
    size_t Read(char* buff, size_t size) override;
    size_t Size() const override { return size_; }
private:
    std::ifstream stream_;
    size_t size_{ 0 };
};

class Client
{
public:
    enum class Auth
    {
        None = 0,
        Basic = CURLAUTH_BASIC,
        Digest = CURLAUTH_DIGEST,
        Try = CURLAUTH_BASIC | CURLAUTH_DIGEST
    };

    Client(std::string url, std::string username, std::string password);
    virtual ~Client();

    virtual std::string GetName() const { return "Client"; }
    virtual Color GetColor() const { return {}; }

    std::shared_ptr<Object> New(ObjectType);

    bool Delete(const std::string& href);
    bool Delete(const Object& object);
    std::shared_ptr<Object> Get(const std::string& href);
    bool Get(const std::string& href, Writer& writer);
    bool Put(const Object& object, std::string charset = "utf8");
    bool Put(const std::string& href, Reader& reader);
    bool Propfind(const std::string& body, int depth, Writer& writer);

    Auth auth_{ Auth::Try };
    bool verifySsl_{ true };
    std::string userAgent_;
    std::function<void(const std::string&, const std::string&)> onError_;

protected:
    void Error(const std::string& category, const std::string& message) const;
    bool MakeRequest(const std::string& url,
        const std::string& method,
        int depth,
        const std::string& request,
        const std::string& contentType,
        Writer* response = nullptr);
    bool Upload(const std::string& url,
        const std::string& method,
        Reader& request,
        Writer* response = nullptr);
    virtual bool Init() { return true; }

    CaseInsensitiveMap<std::string> GetHeaders() const;

    std::string url_;

private:
    static size_t ReadCallback(void* contents, size_t size, size_t nmemb, void* userp);
    static size_t WriteCallback(void* contents, size_t size, size_t nmemb, void* userp);
    static size_t HeaderWriteCallback(void* contents, size_t size, size_t nmemb, void* userp);

    void Prepare(CURL* curl, curl_slist* headers);

    std::string username_;
    std::string password_;
    std::string headers_;
};

}
