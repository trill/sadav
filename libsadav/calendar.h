/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <vector>
#include "event.h"
#include "color.h"
#include "client.h"

namespace sa::dav {

class Calendar final : public Client
{
public:
    Calendar(std::string url, std::string username, std::string password);
    bool Propfind();
    // Get the events as sent by the server
    bool GetRawEvents(const DateTime& start, const DateTime& end, Event::Type type, std::vector<Event>& result);
    // Also calculate recurring events
    bool GetEvents(const DateTime& start, const DateTime& end, Event::Type type, std::vector<Event>& result);

    std::string GetName() const override { return name_; }
    Color GetColor() const override { return color_; }
    // Every time anything changes, the ctag must also change.
    // So the data can be cached.
    const std::string& GetCtag() const { return ctag_; }
private:    
    static void AddRecrring(const std::vector<Event>& events, const DateTime& start, const DateTime& end, std::vector<Event>& result);
    bool ParsePropfind(const std::string& body);
    bool ParseReport(const std::string& body, std::vector<Event>& events);
    bool Init() override;

    std::string syncToken_;
    std::string name_;
    std::string ctag_;
    Color color_;
    bool propfind_{ false };
};

}
