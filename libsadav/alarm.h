/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "datetime.h"
#include "cimap.h"
#include <optional>
#include "parseutils.h"

namespace sa::dav {

class Event;

class Alarm
{
public:
    enum class Action
    {
        None = 0,
        Display,
        Audio,
        Email,
        Procedure
    };
    struct Trigger
    {
        DateTime trigger;
        DateTime relevant;
        // Get time left in seconds or a negative value if overdue
        int TimeLeft() const
        {
            return DateTime::Now().time_ - relevant.time_;
        }
    };

    void Parse(std::stringstream& ss);
    void Clear();

    std::optional<Trigger> GetTrigger(const Event& event) const;
    const DateTime& GetRelevant(const Event& event) const;

    Action GetAction() const { return action_; }
    int GetRepeat() const { return repeat_; }
    int GetDuration() const { return duration_; }
    std::string GetSummary(const Event& event) const;
    std::string GetDescription(const Event& event) const;
    const CaseInsensitiveMap<std::string>& GetFields() const { return fields_; }
private:
    // Relevant date time for trigger, can be start, end, due etc.
    static DateTime GetRelevant(const Key& key, const Event& event);
    Action action_{ Action::None };
    int repeat_{ 0 };
    int duration_{ 0 };
    CaseInsensitiveMap<std::string> fields_;
};

}
