/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <cstdint>

namespace sa::dav {

std::string URLEncode(const std::string& url);
std::string URLDecode(const std::string& url);

class URL
{
public:
    explicit URL(const std::string& url);
    [[nodiscard]] const std::string& Scheme() const { return scheme_; }
    [[nodiscard]] const std::string& Host() const { return host_; }
    [[nodiscard]] const std::string& Path() const { return path_; }
    [[nodiscard]] const std::string& Query() const { return query_; }
    [[nodiscard]] uint16_t Port() const;
    [[nodiscard]] std::string SchemeHostPort() const;
    [[nodiscard]] std::string PathQuery() const;
    [[nodiscard]] bool IsDefaultPort() const;
private:
    void Parse(const std::string& url);
    std::string scheme_;
    std::string host_;
    std::string path_;
    std::string query_;
    uint16_t port_{ 0 };
};

}
