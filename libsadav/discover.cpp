/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "discover.h"
#include <pugixml.hpp>
#include "parseutils.h"

namespace sa::dav {

Discover::Discover(std::string url, std::string username, std::string password) :
    Client(std::move(url), std::move(username), std::move(password))
{ }

std::string Discover::UserPrincipal()
{
    static constexpr std::string_view requestXML = R"xml(<?xml version="1.0" encoding="UTF-8"?>
<d:propfind xmlns:d="DAV:">
  <d:prop>
    <d:current-user-principal/>
    <d:principal-URL/>
    <d:resourcetype/>
  </d:prop>
</d:propfind>)xml";
    StringWriter writer;
    if (!MakeRequest(url_, "PROPFIND", 0, std::string(requestXML), "text/xml; charset=utf-8", &writer))
        return {};

    pugi::xml_document doc;
    const pugi::xml_parse_result result = doc.load_string(writer.contents_.c_str());
    if (result.status != pugi::status_ok)
    {
        Error("xml", result.description());
        return {};
    }
    auto prop = doc.first_element_by_path("d:multistatus/d:response/d:propstat/d:prop/d:current-user-principal/d:href");
    if (!prop)
    {
        Error("dav", "No current-user-principal element found");
        return {};
    }
    return prop.text().as_string();
}

std::vector<std::string> Discover::GetFeatures()
{
    if (!MakeRequest(url_, "OPTIONS", 0, "", "text/plain; charset=utf-8", nullptr))
        return {};

    auto headers = GetHeaders();
    auto it = headers.find("DAV");
    if (it == headers.end())
        return {};
    auto parts = Split(it->second, ",");
    std::vector<std::string> result;
    result.reserve(parts.size());
    for (const auto& feat : parts)
        result.push_back(Trim<char>(feat));
    return result;
}

std::vector<std::pair<std::string, std::string>> Discover::List()
{
    static constexpr std::string_view requestXML = R"xml(<?xml version="1.0" encoding="UTF-8"?>
<d:propfind xmlns:d="DAV:">
<d:prop>
    <d:displayname />
    <d:resourcetype />
  </d:prop>
</d:propfind>)xml";
    StringWriter writer;
    if (!MakeRequest(url_, "PROPFIND", 1, std::string(requestXML), "text/xml; charset=utf-8", &writer))
        return {};

    std::vector<std::pair<std::string, std::string>> result;
    pugi::xml_document doc;
    const pugi::xml_parse_result xmlresult = doc.load_string(writer.contents_.c_str());
    if (xmlresult.status != pugi::status_ok)
    {
        Error("xml", xmlresult.description());
        return {};
    }

    auto responses = doc.first_element_by_path("d:multistatus");
    if (!responses)
    {
        Error("dav", "multistatus element not found");
        return {};
    }

    for (const auto& response : responses.children("d:response"))
    {
        auto elStatus = response.first_element_by_path("d:propstat/d:status");
        if (!elStatus)
        {
            Error("dav", "No status element");
            return {};
        }
        if (!std::string(elStatus.text().as_string()).ends_with("200 OK"))
            continue;

        std::string href;
        std::string displayName;

        auto elHref = response.child("d:href");
        if (!elHref)
            continue;

        href = elHref.text().as_string();

        // Looking for collections
        auto rt = response.first_element_by_path("d:propstat/d:prop/d:resourcetype/d:collection");
        if (!rt)
            continue;

        auto elDisplayName = response.first_element_by_path("d:propstat/d:prop/d:displayname");
        if (elDisplayName)
            displayName = elDisplayName.text().as_string();
        result.push_back(std::make_pair(displayName, href));

    }
    return result;
}

}
