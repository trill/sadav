/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "parseutils.h"

namespace sa::dav {

Key ParseKey(const std::string& key)
{
    auto parts = Split(key, ";");
    if (parts.size() == 1)
        return { .name = key, .values = {} };

    Key result{ .name = parts[0], .values = {} };
    for (auto it = parts.begin() + 1; it != parts.end(); ++it)
    {
        auto keyvalue = Split(*it, "=");
        if (keyvalue.size() == 2)
        {
            result.values.emplace(keyvalue[0], keyvalue[1]);
        }
    }
    return result;
}

bool NextField(std::stringstream& ss, std::string& result)
{
    /*
Lines of text SHOULD NOT be longer than 75 octets, excluding the line
break.  Long content lines SHOULD be split into a multiple line
representations using a line "folding" technique.  That is, a long
line can be split between any two characters by inserting a CRLF
immediately followed by a single linear white-space character (i.e.,
SPACE or HTAB)
     */
    result.clear();
    std::string line;
    auto pos = ss.tellg();
    bool first = true;
    while (std::getline(ss, line))
    {
        if (line.empty())
            continue;

        if (first)
        {
            first = false;
        }
        else
        {
            if (!line.starts_with(' ') && !line.starts_with('\t'))
            {
                ss.seekg(pos, std::ios::beg);
                return true;
            }
        }
        if (!result.empty())
            result.append("\r\n");
        result.append(RightTrim<char>(line, "\r\n"));
        pos = ss.tellg();
    }
    return !result.empty();
}

std::optional<int> ParseDuration(const std::string& value)
{
    if (value.length() < 2)
        return {};
    char sign = '+';
    size_t start = 0;
    if (value[0] == '+')
    {
        ++start;
    }
    else if (value[0] == '-')
    {
        sign = '-';
        ++start;
    }
    if (value[start] != 'P')
        return {};
    ++start;
    int result = 0;
    size_t vstart = start;
    for (size_t p = start; p < value.length(); ++p)
    {
        char c = value[p];
        switch (c)
        {
        case 'W':
        {
            auto num = ToNumber<int>(value.substr(vstart, p - vstart));
            if (!num.has_value())
                return {};
            result += *num * SECONDS_PER_WEEK;
            vstart = p + 1;
            break;
        }
        case 'H':
        {
            auto num = ToNumber<int>(value.substr(vstart, p - vstart));
            if (!num.has_value())
                return {};
            result += *num * SECONDS_PER_HOUR;
            vstart = p + 1;
            break;
        }
        case 'M':
        {
            auto num = ToNumber<int>(value.substr(vstart, p - vstart));
            if (!num.has_value())
                return {};
            result += *num * SECONDS_PER_MINUTE;
            vstart = p + 1;
            break;
        }
        case 'S':
        {
            auto num = ToNumber<int>(value.substr(vstart, p - vstart));
            if (!num.has_value())
                return {};
            result += *num;
            vstart = p + 1;
            break;
        }
        case 'T':
            vstart = p + 1;
            break;
        case 'D':
        {
            auto num = ToNumber<int>(value.substr(vstart, p - vstart));
            if (!num.has_value())
                return {};
            result += *num * SECONDS_PER_DAY;
            vstart = p + 1;
            break;
        }
        default:
            break;
        }

    }

    return (sign == '+') ? result : -result;
}

bool IsWhite(const std::string& s)
{
    std::string::const_iterator it = s.begin();
    while (it != s.end() && std::isspace(*it)) ++it;
    return !s.empty() && it == s.end();
}

std::vector<std::string> Split(const std::string& str, const std::string& delim, bool keepEmpty, bool keepWhite)
{
    std::vector<std::string> result;

    size_t beg = 0;
    for (size_t end = 0; (end = str.find(delim, end)) != std::string::npos; ++end)
    {
        auto substring = str.substr(beg, end - beg);
        beg = end + delim.length();
        if (!keepEmpty && substring.empty())
            continue;
        if (!keepWhite && IsWhite(substring))
            continue;
        result.push_back(substring);
    }

    auto substring = str.substr(beg);
    if (substring.empty())
    {
        if (keepEmpty)
            result.push_back(substring);
    }
    else if (IsWhite(substring))
    {
        if (keepWhite)
            result.push_back(substring);
    }
    else
        result.push_back(substring);

    return result;
}

std::string ProcessString(const std::string& value)
{
    std::string result = value;
    ReplaceSubstring<char>(result, "\n", " ");
    ReplaceSubstring<char>(result, "\r", " ");
    while (ReplaceSubstring<char>(result, "  ", " "));
    ReplaceSubstring<char>(result, "\\n", "\n");
    return result;
}

}
