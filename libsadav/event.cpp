/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "event.h"
#include <iostream>

namespace sa::dav {

Event::Event(std::string href, std::string etag, std::string calendar, Color color) :
    Event(Type::Event, std::move(href), std::move(etag), std::move(calendar), color)
{ }

Event::Event(Type type, std::string href, std::string etag, std::string calendar, Color color) :
    Object(std::move(href), std::move(etag), std::move(calendar), "2.0"),
    stamp_(DateTime::Now()),
    color_(color),
    type_(type)
{
    if (type_ == Event::Type::Event)
        start_ = DateTime::Now();
    stamp_.tz_ = "";
}

void Event::Clear()
{
    Object::Clear();
    summary_.clear();
    start_ = {};
    end_ = {};
    stamp_ = {};
    due_ = {};
    rrule_ = RRule();
    exdates_.clear();
    alarms_.clear();
    other_.clear();
}

bool Event::Parse(const std::string& data)
{
    Clear();
    std::stringstream ss(data);
    std::string line;
    bool haveVersion = false;
    bool haveBegin = false;

    while (NextField(ss, line))
    {
        if (line.find("VERSION:") != std::string::npos)
        {
            auto colonPos = line.find(':');
            std::string key = line.substr(0, colonPos);
            std::string value = line.substr(colonPos + 1);
            version_ = value;
            haveVersion = true;
        }
        else if (line.find("PRODID:") != std::string::npos)
        {
            auto colonPos = line.find(':');
            std::string key = line.substr(0, colonPos);
            std::string value = line.substr(colonPos + 1);
            prodid_ = value;
        }
        else if (line.find("BEGIN:VEVENT") != std::string::npos)
        {
            type_ = Type::Event;
            haveBegin = true;
            ParseEvent(ss);
            break;
        }
        else if (line.find("BEGIN:VTODO") != std::string::npos)
        {
            type_ = Type::Todo;
            haveBegin = true;
            ParseEvent(ss);
            break;
        }
        else if (line.find("BEGIN:VJOURNAL") != std::string::npos)
        {
            type_ = Type::Journal;
            haveBegin = true;
            ParseEvent(ss);
            break;
        }
    }

    if (!haveVersion)
        return false;
    if (!haveBegin)
        return false;
    if (type_ == Event::Type::Event && start_.Empty())
        return false;

    return true;
}

void Event::ParseEvent(std::stringstream& ss)
{
    // https://en.wikipedia.org/wiki/ICalendar
    std::string line;

    auto isEnd = [&](const std::string& l)
    {
        switch (type_)
        {
        case Event::Type::Event:
            return l.find("END:VEVENT") != std::string::npos;
        case Event::Type::Todo:
            return l.find("END:VTODO") != std::string::npos;
        case Event::Type::Journal:
            return l.find("END:VJOURNAL") != std::string::npos;
        }
        return false;
    };

    while (NextField(ss, line))
    {
        if (isEnd(line))
            break;

        auto colonPos = line.find(':');
        if (colonPos == std::string::npos)
            continue;

        std::string key = line.substr(0, colonPos);
        std::string value = line.substr(colonPos + 1);
        Key parsedKey = ParseKey(key);

        if (parsedKey.name == "BEGIN")
        {
            if (value == "VALARM")
            {
                Alarm alarm;
                alarm.Parse(ss);
                alarms_.push_back(std::move(alarm));
            }
        }
        else if (parsedKey.name == "DTSTAMP")
            ParseDateTime(key, value, stamp_);
        else if (parsedKey.name == "DTSTART")
            ParseDateTime(key, value, start_);
        else if (parsedKey.name == "DTEND")
            ParseDateTime(key, value, end_);
        else if (parsedKey.name == "CREATED")
            ParseDateTime(key, value, created_);
        else if (parsedKey.name == "LAST-MODIFIED")
            modified_.Parse(value);
        else if (parsedKey.name == "DUE")
            ParseDateTime(key, value, due_);
        else if (parsedKey.name == "SUMMARY")
            summary_ = value;
        else if (parsedKey.name == "RRULE")
        {
            RRule r(value);
            if (r.IsValid())
                std::swap(rrule_, r);
        }
        else if (parsedKey.name == "EXDATE")
        {
            std::string tz;
            if (!parsedKey.values.empty())
            {
                if (auto tzIt = parsedKey.values.find("TZID"); tzIt != parsedKey.values.end())
                    tz = tzIt->second;
            }
            AddExdate(value, tz);
        }

        else if (parsedKey.name == "UID")
            uid_ = value;
        else
            other_.emplace(key, value);
    }
}

void Event::AddExdate(const std::string& value, const std::string& tz)
{
    auto values = Split(value, ",", false, false);
    for (const auto& v : values)
    {
        DateTime e;
        if (e.Parse(v, tz))
            exdates_.push_back(e);
    }
    std::sort(exdates_.begin(), exdates_.end());
    exdates_.erase(std::unique(exdates_.begin(), exdates_.end()), exdates_.end());
}

bool Event::IsExcluded(const DateTime& date) const
{
    for (const auto& e : exdates_)
    {
        if (date.SameDate(e))
            return true;
    }
    return false;
}

std::string Event::ToString() const
{
    std::stringstream ss;
    ss << "VERSION:" << version_ << "\r\n";
    ss << "PRODID:" << prodid_ << "\r\n";

    switch (type_)
    {
    case Event::Type::Event:
        ss << "BEGIN:VEVENT\r\n";
        break;
    case Event::Type::Todo:
        ss << "BEGIN:VTODO\r\n";
        break;
    case Event::Type::Journal:
        ss << "BEGIN:VJOURNAL\r\n";
        break;
    }

    ss << "UID:" << uid_ << "\r\n";
    ss << "DTSTAMP" << stamp_ << "\r\n";
    if (!created_.Empty())
        ss << "CREATED" << created_ << "\r\n";
    if (!start_.Empty())
        ss << "DTSTART" << start_ << "\r\n";
    if (!end_.Empty())
        ss << "DTEND" << end_ << "\r\n";
    if (!due_.Empty())
        ss << "DUE" << due_ << "\r\n";
    if (!summary_.empty())
        ss << "SUMMARY:" << summary_ << "\r\n";
    if (!rrule_.AsString().empty())
        ss << "RRULE:" << rrule_.AsString() << "\r\n";
    if (!exdates_.empty())
    {
        ss << "EXDATE";
        if (!exdates_.front().tz_.empty())
            ss << ";TZID=" << exdates_.front();
        ss << ":";
        bool first = true;
        for (const auto& ec : exdates_)
        {
            if (first)
                first = false;
            else
                ss << ",";
            ss << ec.ToString();
        }
        ss << "\r\n";
    }
    if (!alarms_.empty())
    {
        for (const auto& al : alarms_)
        {
            ss << "BEGIN:VALARM\r\n";
            for (const auto& o : al.GetFields())
                ss << o.first << ":" << o.second << "\r\n";
            ss << "END:VALARM\r\n";
        }
    }
    for (const auto& other : other_)
        ss << other.first << ":" << other.second << "\r\n";

    switch (type_)
    {
    case Event::Type::Event:
        ss << "END:VEVENT\r\n";
        break;
    case Event::Type::Todo:
        ss << "END:VTODO\r\n";
        break;
    case Event::Type::Journal:
        ss << "END:VJOURNAL\r\n";
        break;
    }

    return ss.str();
}

void Event::AddRecurring(const DateTime& start, const DateTime& end, std::vector<Event>& events) const
{
    if (!IsRecurring())
        return;
    rrule_.GetRecurringDates(*this, start, end, [&](const DateTime& date)
    {
        auto copy = *this;
        copy.start_.SetDate(date);
        copy.end_.SetDate(date);
        events.push_back(std::move(copy));
    });
}

bool Event::IsAllDay() const
{
    if (start_.value_ == DateTime::Value::Date)
    {
        if (end_.Empty())
            return true;
        return end_.value_ == DateTime::Value::Date;
    }
    return false;
}

void Event::MakeAllDay()
{
    start_.value_ = DateTime::Value::Date;
    end_.value_ = DateTime::Value::Date;
}

bool Event::IsOnDay(const DateTime& day) const
{
    if (start_.Empty())
        return false;
    auto d = day.Date();
    if (end_.Empty())
        return start_.Date() == d;

    return d >= start_.Date() && d <= end_.Date();
}

}
