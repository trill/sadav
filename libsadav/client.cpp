/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "client.h"
#include "httpstatus.h"
#include "parseutils.h"
#include "url.h"
#include "event.h"
#include "contact.h"
#include "uuid.h"
#include "config.h"
#include <cstring>

namespace sa::dav {

Writer::~Writer() = default;

void StringWriter::Write(char* data, size_t size)
{
    contents_.append(data, size);
}

FileWriter::FileWriter(const std::string& filename)
{
    stream_.open(filename);
}

void FileWriter::Write(char* data, size_t size)
{
    if (stream_.is_open())
        stream_.write((const char*)data, size);
}

Reader::~Reader() = default;

StringReader::StringReader(std::string content) :
    content_(std::move(content))
{ }

StringReader::StringReader(const char* content) :
    content_(content)
{ }

size_t StringReader::Read(char* buff, size_t size)
{
    if (pos_ >= content_.length())
        return 0;
    size_t avail = content_.length() - pos_;
    size_t read = std::min(avail, size);
    strncpy(buff, content_.data() + pos_, read);
    pos_ += read;
    return read;
}

size_t StringReader::Size() const
{
    return content_.length();
}

FileReader::FileReader(const std::string& filename)
{
    stream_.open(filename);
    if (stream_.is_open())
    {
        stream_.seekg(0, std::ios::end);
        size_ = stream_.tellg();
        stream_.seekg(0, std::ios::beg);
    }
}

size_t FileReader::Read(char* buff, size_t size)
{
    if (!stream_.is_open())
        return 0;
    stream_.read(buff, size);
    return stream_.gcount();
}

Client::Client(std::string url, std::string username, std::string password) :
    userAgent_(Config::Instance().Useragent()),
    url_(std::move(url)),
    username_(std::move(username)),
    password_(std::move(password))
{
    // Make sure we have a trailing slash
    if (!url_.empty() && !url_.ends_with('/'))
        url_ += '/';
}

Client::~Client() = default;

void Client::Error(const std::string& category, const std::string& message) const
{
    if (onError_)
        onError_(category, message);
}

size_t Client::ReadCallback(void* contents, size_t size, size_t nmemb, void* userp)
{
    if (userp != nullptr)
        return static_cast<Reader*>(userp)->Read((char*)contents, size * nmemb);
    return size * nmemb;
}

size_t Client::WriteCallback(void* contents, size_t size, size_t nmemb, void* userp)
{
    if (userp != nullptr)
        static_cast<Writer*>(userp)->Write((char*)contents, size * nmemb);
    return size * nmemb;
}

size_t Client::HeaderWriteCallback(void* contents, size_t size, size_t nmemb, void* userp)
{
    static_cast<Client*>(userp)->headers_.append((char*)contents, size * nmemb);
    return size * nmemb;
}

CaseInsensitiveMap<std::string> Client::GetHeaders() const
{
    if (headers_.empty())
        return {};

    CaseInsensitiveMap<std::string> result;
    std::stringstream ss(headers_);
    std::string line;
    while (std::getline(ss, line))
    {
        auto parts = Split(Trim<char>(line, " \t\r\n"), ":");
        if (parts.size() == 2)
            result.emplace(parts[0], parts[1]);
    }
    return result;
}

void Client::Prepare(CURL* curl, curl_slist* headers)
{
    const std::string userAgent = std::format("User-Agent: {}", userAgent_.c_str());
    headers = curl_slist_append(headers, userAgent.c_str());
    headers = curl_slist_append(headers, "Prefer: return-minimal");
    (void)headers;

    if (!verifySsl_)
    {
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
    }
    const std::string userpwd = std::format("{}:{}", username_, password_);
    if (auth_ != Auth::None)
        curl_easy_setopt(curl, CURLOPT_HTTPAUTH, static_cast<int>(auth_));
    curl_easy_setopt(curl, CURLOPT_USERPWD, userpwd.c_str());
    curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, HeaderWriteCallback);
    curl_easy_setopt(curl, CURLOPT_HEADERDATA, this);
}

bool Client::Upload(const std::string& url,
    const std::string& method,
    Reader& request,
    Writer* response)
{
    if (url.empty())
    {
        Error("curl", "URL can not be empty");
        return false;
    }
    if (request.Size() == 0)
    {
        Error("curl", "Request body is empty");
        return false;
    }

    headers_.clear();
    auto* curl = curl_easy_init();
    if (!curl)
    {
        Error("curl", "curl_easy_init() failed");
        return false;
    }

    const std::string contentLength = std::format("Content-Length: {}", request.Size());
    const std::string contentT = std::format("Content-Type: {}", request.contentType_.empty() ? "application/octet-stream" : request.contentType_);
    const std::string userAgent = std::format("User-Agent: {}", userAgent_.c_str());
    curl_slist* headers = nullptr;
    headers = curl_slist_append(headers, contentT.c_str());
    headers = curl_slist_append(headers, contentLength.c_str());
    Prepare(curl, headers);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
    curl_easy_setopt(curl, CURLOPT_READFUNCTION, ReadCallback);
    curl_easy_setopt(curl, CURLOPT_READDATA, &request);
    curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, method.c_str());
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, response);
    CURLcode res = curl_easy_perform(curl);
    int httpStatus = 0;
    curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpStatus);
    curl_slist_free_all(headers);
    curl_easy_cleanup(curl);

    if (res != CURLE_OK)
    {
        Error("curl", curl_easy_strerror(res));
        return false;
    }
    if (httpStatus < 100)
    {
        // Interesting, it returned CURLE_OK
        Error("curl", "Unable to connect");
        return false;
    }
    if (httpStatus >= 400)
    {
        Error("http", std::format("{} {}", httpStatus, sa::http::StatusMessage(httpStatus)));
        return false;
    }
    return true;
}

bool Client::MakeRequest(const std::string& url,
    const std::string& method,
    int depth,
    const std::string& request,
    const std::string& contentType,
    Writer* response)
{
    if (url.empty())
    {
        Error("curl", "URL can not be empty");
        return false;
    }

    headers_.clear();
    auto* curl = curl_easy_init();
    if (!curl)
    {
        Error("curl", "curl_easy_init() failed");
        return false;
    }

    const std::string contentLength = std::format("Content-Length: {}", request.length());
    const std::string contentT = std::format("Content-Type: {}", contentType);
    const std::string sdepth = std::format("Depth: {}", depth);
    curl_slist* headers = nullptr;
    headers = curl_slist_append(headers, sdepth.c_str());
    if (!contentType.empty())
        headers = curl_slist_append(headers, contentT.c_str());
    if (request.length() != 0)
        headers = curl_slist_append(headers, contentLength.c_str());
    Prepare(curl, headers);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    if (!request.empty())
    {
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, request.c_str());
        curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, request.length());
    }
    else
    {
        curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L);
    }
    curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, method.c_str());
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, response);
    CURLcode res = curl_easy_perform(curl);
    int httpStatus = 0;
    curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpStatus);
    curl_slist_free_all(headers);
    curl_easy_cleanup(curl);

    if (res != CURLE_OK)
    {
        Error("curl", curl_easy_strerror(res));
        return false;
    }
    if (httpStatus < 100)
    {
        // Interesting, it returned CURLE_OK
        Error("curl", "Unable to connect");
        return false;
    }
    if (httpStatus >= 400)
    {
        Error("http", std::format("{} {}", httpStatus, sa::http::StatusMessage(httpStatus)));
        return false;
    }
    return true;
}

bool Client::Delete(const std::string& href)
{
    sa::dav::URL _url(url_);
    std::string objectUrl = _url.SchemeHostPort() + href;
    return MakeRequest(objectUrl, "DELETE", 0, "", "", nullptr);
}

bool Client::Delete(const Object& object)
{
    return Delete(object.GetHref());
}

bool Client::Get(const std::string& href, Writer& writer)
{
    URL url(url_);
    std::string objectUrl = url.SchemeHostPort() + href;
    return MakeRequest(objectUrl, "GET", 0, "", "", &writer);
}

bool Client::Put(const std::string& href, Reader& reader)
{
    URL url(url_);
    std::string objectUrl = url.SchemeHostPort() + href;
    return Upload(objectUrl, "PUT", reader);
}

bool Client::Propfind(const std::string& body, int depth, Writer& writer)
{
    return MakeRequest(url_, "PROPFIND", depth, body, "text/xml; charset=utf-8", &writer);
}

std::shared_ptr<Object> Client::Get(const std::string& href)
{
    if (!Init())
        return {};
    StringWriter writer;
    if (!Get(href, writer))
        return {};

    auto type = DetectObjectType(writer.contents_);
    if (type == ObjectType::Unknown)
        return {};
    if (type == ObjectType::Event || type == ObjectType::Todo || type == ObjectType::Journal)
    {
        auto result = std::make_shared<Event>(href, "", GetName(), GetColor());
        if (!result->Parse(writer.contents_))
            return {};
        return result;
    }
    if (type == ObjectType::Contact)
    {
        auto result = std::make_shared<Contact>(href, "", GetName());
        if (!result->Parse(writer.contents_))
            return {};
        return result;
    }
    return {};
}

bool Client::Put(const Object& object, std::string charset)
{
    URL url(url_);
    std::string requestUrl = url.SchemeHostPort() + object.GetHref();
    if (const auto* event = dynamic_cast<const Event*>(&object))
    {
        std::stringstream ss;
        ss << "BEGIN:VCALENDAR\r\n";
        ss << event->ToString();
        ss << "END:VCALENDAR\r\n";
        std::string contentType = std::format("text/calendar; charset={}", charset);
        return MakeRequest(requestUrl, "PUT", 0, ss.str(), contentType, nullptr);
    }
    if (const auto* contact = dynamic_cast<const Contact*>(&object))
    {
        std::string contentType = std::format("text/vcard; charset={}", charset);
        return MakeRequest(requestUrl, "PUT", 0, contact->ToString(), contentType, nullptr);
    }
    return false;
}

std::shared_ptr<Object> Client::New(ObjectType type)
{
    if (!Init())
        return {};
    if (type == ObjectType::Event || type == ObjectType::Todo || type == ObjectType::Journal)
    {
        URL url(url_);
        std::string href = url.Path() + RandomUUID() + ".ics";
        Event::Type t = Event::Type::Event;
        if (type == ObjectType::Todo)
            t = Event::Type::Todo;
        else if (type == ObjectType::Journal)
            t = Event::Type::Journal;
        return std::make_shared<Event>(t, std::move(href), "", GetName(), GetColor());
    }
    if (type == ObjectType::Contact)
    {
        URL url(url_);
        std::string href = url.Path() + RandomUUID() + ".vcf";
        return std::make_shared<Contact>(std::move(href), "", GetName());
    }
    return {};
}

}
