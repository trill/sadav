/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "color.h"
#include <charconv>
#include <array>
#include <random>

namespace sa::dav {

Color Color::FromString(const std::string& value)
{
    Color result;
    size_t start = value.front() == '#' ? 1 : 0;
    if ((value.length() - start) % 2 != 0)
        return {};

    const size_t count = (value.length() - start) / 2;
    std::from_chars(value.data() + start, value.data() + start + 2, result.r, 16);
    if (count > 1)
    {
        ++start; ++start;
        std::from_chars(value.data() + start, value.data() + start + 2, result.g, 16);
    }
    if (count > 2)
    {
        ++start; ++start;
        std::from_chars(value.data() + start, value.data() + start + 2, result.b, 16);
    }
    return result;
}

Color Color::Random()
{
    Color result;
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<uint8_t> uid(0, 255);
    result.r = uid(rng);
    result.g = uid(rng);
    result.b = uid(rng);

    return result;
}

std::string Color::ToString() const
{
    std::array<char, 16> buff;
    sprintf(buff.data(), "%02x%02x%02x", r, g, b);
    return buff.data();
}

}
