/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>

namespace sa::dav {

class DateTime;

enum class ObjectType
{
    Unknown,
    Event,
    Todo,
    Journal,
    Contact
};

ObjectType DetectObjectType(const std::string& contents);
bool ParseDateTime(const std::string& key, const std::string& value, DateTime& result);

class Object
{
public:
    Object(std::string href, std::string etag, std::string owner, std::string version);
    virtual ~Object();

    virtual bool Parse(const std::string& data) = 0;
    virtual std::string ToString() const = 0;
    virtual void Clear();

    const std::string& GetOwner() const { return owner_; }
    const std::string& GetUID() const { return uid_; }
    const std::string& GetHref() const { return href_; }
    const std::string& GetEtag() const { return etag_; }
    std::string GetFilename() const;

    std::string prodid_;
    std::string version_;
protected:
    std::string uid_;
    std::string href_;
    std::string etag_;
    std::string owner_;
};

}
