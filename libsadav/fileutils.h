/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>

namespace sa::dav {

bool WriteFile(const std::string& filename, const std::string& contents);
std::string ReadFile(const std::string& filename);
std::string ExpandPath(const std::string& path);

}
