/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "config.h"
#include <date/tz.h>
#include "version.h"

namespace sa::dav {

static constexpr const char* USER_AGENT = "-//sa//libsadav " SADAV_VERSION_STRING "//EN";

Config& Config::Instance()
{
    static Config instance;
    return instance;
}

Config::Config() :
    useragent_(USER_AGENT),
    timezone_(date::current_zone()->name())
{ }

void Config::SetUseragent(std::string value)
{
    useragent_ = std::move(value);
}

void Config::SetTimezone(std::string value)
{
    timezone_ = std::move(value);
}

}
