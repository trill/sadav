/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <sstream>
#include <string>
#include <vector>
#include "cimap.h"
#include "color.h"
#include "datetime.h"
#include "object.h"
#include "rrule.h"
#include "alarm.h"
#include <optional>

namespace sa::dav {

// https://icalendar.org/RFC-Specifications/iCalendar-RFC-5545/
class Event final : public Object
{
public:
    enum class Type
    {
        Event,
        Todo,
        Journal
    };

    Event(Type type, std::string href, std::string etag, std::string calendar, Color color);
    Event(std::string href, std::string etag, std::string calendar, Color color);

    bool Parse(const std::string& data) override;
    bool IsExcluded(const DateTime& date) const;
    std::string ToString() const override;
    void Clear() override;
    bool IsRecurring() const { return rrule_.IsValid(); }
    void AddRecurring(const DateTime& start, const DateTime& end, std::vector<Event>& events) const;
    std::string Format(const std::string& templ);
    Type GetType() const { return type_; }
    const Color& GetColor() const { return color_; }
    bool IsAllDay() const;
    void MakeAllDay();
    bool IsOnDay(const DateTime& day) const;
    template<typename Callback>
    void GetCurrentAlarams(Callback callback) const
    {
        auto now = DateTime::Now();
        for (const auto& a : alarms_)
        {
            auto trigger = a.GetTrigger(*this);
            if (trigger.has_value())
            {
                if (now >= trigger->trigger && now < trigger->relevant)
                    callback(a, *trigger);
            }
        }
    }

    DateTime stamp_;
    DateTime created_;
    DateTime modified_;

    std::string summary_;
    DateTime start_;
    DateTime end_;
    DateTime due_;
    RRule rrule_;
    std::vector<DateTime> exdates_;
    std::vector<Alarm> alarms_;

    // All not needed fields for our purpose
    CaseInsensitiveMap<std::string> other_;

private:
    void ParseEvent(std::stringstream& ss);
    void AddExdate(const std::string& value, const std::string& tz = "");

    Color color_;
    Type type_{ Type::Event };
};

}
