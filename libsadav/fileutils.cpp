/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "fileutils.h"
#include <fstream>
#include <sstream>
#if __has_include(<wordexp.h>)
// Linux only
#include <wordexp.h>
#define HAVE_WORDEXP
#endif
#include <parseutils.h>

namespace sa::dav {

bool WriteFile(const std::string& filename, const std::string& contents)
{
    std::ofstream f(filename);
    if (!f.is_open())
        return false;
    f << contents;
    return true;
}

std::string ReadFile(const std::string& filename)
{
    std::ifstream f(filename);
    if (!f.is_open())
        return "";
    std::stringstream ss;
    ss << f.rdbuf();
    return ss.str();
}

std::string ExpandPath(const std::string& path)
{
#ifdef HAVE_WORDEXP
    wordexp_t p;
    wordexp(path.c_str(), &p, 0);
    std::string result;
    if (p.we_wordc != 0)
    {
        result = p.we_wordv[0];
    }
    wordfree(&p);
    return result;
#else
    const char* home = getenv("HOME");
    if (!home)
        return path;
    std::string result = path;
    sa::dav::ReplaceSubstring<char>(result, "~", std::string(home));
    return result;
#endif
}

}
