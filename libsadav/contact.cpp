/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "contact.h"
#include <optional>
#include "parseutils.h"

namespace sa::dav {

Contact::Contact(std::string href, std::string etag, std::string addressbook) :
    Object(std::move(href), std::move(etag), std::move(addressbook), "3.0")
{
}

void Contact::Clear()
{
    Object::Clear();
    formattedName_.clear();
    name_.clear();
    emails_.clear();
    tels_.clear();
    addresses_.clear();
    bday_ = {};
    messaging_.clear();
    other_.clear();
}

bool Contact::Parse(const std::string& data)
{
    Clear();
    std::stringstream ss(data);
    std::string line;
    bool haveBegin = false;
    while (std::getline(ss, line))
    {
        if (line.find("BEGIN:VCARD") != std::string::npos)
        {
            ParseContact(ss);
            haveBegin = true;
            break;
        }
    }

    if (!haveBegin)
        return false;
    if (name_.empty())
        return false;

    return true;
}

void Contact::ParseContact(std::stringstream& ss)
{
    // https://en.wikipedia.org/wiki/VCard
    std::string field;
    while (NextField(ss, field))
    {
        if (field.find("END:VCARD") != std::string::npos)
            break;

        auto colonPos = field.find(':');
        if (colonPos == std::string::npos)
            continue;

        std::string key = field.substr(0, colonPos);
        std::string value = field.substr(colonPos + 1);
        Key parsedKey = ParseKey(key);

        if (parsedKey.name == "FN")
            formattedName_ = value;
        else if (parsedKey.name == "N")
            name_ = value;
        else if (parsedKey.name == "EMAIL")
        {
            if (auto it = parsedKey.values.find("TYPE"); it != parsedKey.values.end())
                emails_.emplace(it->second, value);
            else
                emails_.emplace("", value);
        }
        else if (parsedKey.name == "TEL")
        {
            if (auto it = parsedKey.values.find("TYPE"); it != parsedKey.values.end())
                tels_.emplace(it->second, value);
            else
                tels_.emplace("", value);
        }
        else if (parsedKey.name == "ADR")
        {
            if (auto it = parsedKey.values.find("TYPE"); it != parsedKey.values.end())
                addresses_.emplace(it->second, value);
            else
                addresses_.emplace("", value);
        }
        else if (parsedKey.name == "BDAY")
        {
            if (ParseDateTime(key, value, bday_))
                bday_.value_ = DateTime::Value::Date;
        }
        else if (parsedKey.name == "IMPP")
            messaging_.push_back(value);

        else if (parsedKey.name == "PRODID")
            prodid_ = value;
        else if (parsedKey.name == "VERSION")
            version_ = value;
        else if (parsedKey.name == "UID")
            uid_ = value;
        else
            other_.emplace(key, value);
    }
}

std::string Contact::ToString() const
{
    std::stringstream ss;
    ss << "BEGIN:VCARD\r\n";
    ss << "VERSION:" << version_ << "\r\n";
    ss << "PRODID:" << prodid_ << "\r\n";
    ss << "UID:" << uid_ << "\r\n";
    ss << "N:" << name_ << "\r\n";
    if (!formattedName_.empty())
        ss << "FN:" << formattedName_ << "\r\n";
    if (!addresses_.empty())
    {
        for (const auto& a : addresses_)
        {
            if (a.first.empty())
                ss << "ADR:" << a.second << "\r\n";
            else
                ss << "ADR;TYPE=" << a.first << ":" << a.second << "\r\n";
        }
    }
    if (!emails_.empty())
    {
        for (const auto& a : emails_)
        {
            if (a.first.empty())
                ss << "EMAIL:" << a.second << "\r\n";
            else
                ss << "EMAIL;TYPE=" << a.first << ":" << a.second << "\r\n";
        }
    }
    if (!tels_.empty())
    {
        for (const auto& a : tels_)
        {
            if (a.first.empty())
                ss << "TEL:" << a.second << "\r\n";
            else
                ss << "TEL;TYPE=" << a.first << ":" << a.second << "\r\n";
        }
    }
    if (!bday_.Empty())
        ss << "BDAY:" << bday_.ToString() << "\r\n";
    for (const auto& msg : messaging_)
        ss << "IMPP:" << msg << "\r\n";
    for (const auto& other : other_)
        ss << other.first << ":" << other.second << "\r\n";
    ss << "END:VCARD\r\n";
    return ss.str();
}

}
