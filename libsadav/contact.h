/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <vector>
#include "datetime.h"
#include "object.h"
#include "cimap.h"

namespace sa::dav {

// https://en.wikipedia.org/wiki/VCard
class Contact final : public Object
{
public:
    Contact(std::string href, std::string etag, std::string addressbook);
    bool Parse(const std::string& data) override;
    std::string ToString() const override;
    void Clear() override;

    std::string formattedName_;
    std::string name_;
    DateTime bday_;
    CaseInsensitiveMap<std::string> emails_;
    CaseInsensitiveMap<std::string> tels_;
    CaseInsensitiveMap<std::string> addresses_;
    std::vector<std::string> messaging_;

    CaseInsensitiveMap<std::string> other_;

private:
    void ParseContact(std::stringstream& ss);
};

}
