/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "calendar.h"
#include <cstring>
#include <iostream>
#include <ostream>
#include <pugixml.hpp>
#include <string_view>

namespace sa::dav {

Calendar::Calendar(std::string url, std::string username, std::string password) :
    Client(std::move(url), std::move(username), std::move(password)),
    color_(Color::Random())
{
}

bool Calendar::GetRawEvents(const DateTime& start, const DateTime& end, Event::Type type, std::vector<Event>& result)
{
    if (!Init())
        return false;

    if (start.Date() >= end.Date())
    {
        Error("dav", "End date must be after start date");
        return {};
    }

    static constexpr std::string_view requestXML = R"xml(<?xml version="1.0" encoding="UTF-8"?>
<cal:calendar-query xmlns:d="DAV:" xmlns:cal="urn:ietf:params:xml:ns:caldav">
    <d:prop>
        <d:getetag />
        <cal:calendar-data />
    </d:prop>
    <cal:filter>
        <cal:comp-filter name='VCALENDAR'>
            <cal:comp-filter name='{}'>
                <cal:time-range start='{}' end='{}'/>
            </cal:comp-filter>
        </cal:comp-filter>
    </cal:filter>
</cal:calendar-query>)xml";
    std::string comp;
    switch (type)
    {
    case Event::Type::Event:
        comp = "VEVENT";
        break;
    case Event::Type::Todo:
        comp = "VTODO";
        break;
    case Event::Type::Journal:
        comp = "VJOURNAL";
        break;
    default:
        return true;
    }

    StringWriter writer;
    std::string request = std::format(requestXML, comp, start.ToString(), end.ToString());
    if (!MakeRequest(url_, "REPORT", 1, request, "text/xml; charset=utf-8", &writer))
        return {};

    return ParseReport(writer.contents_, result);
}

bool Calendar::GetEvents(const DateTime& start, const DateTime& end, Event::Type type, std::vector<Event>& result)
{
    std::vector<Event> raw;
    if (!GetRawEvents(start, end, type, raw))
        return false;
    AddRecrring(raw, start, end, result);
    return true;
}

void Calendar::AddRecrring(const std::vector<Event>& events, const DateTime& start, const DateTime& end, std::vector<Event>& result)
{
    for (const auto& event : events)
    {
        if (!event.IsRecurring())
            result.push_back(event);
        else
            event.AddRecurring(start, end, result);
    }
}

bool Calendar::ParseReport(const std::string& body, std::vector<Event>& events)
{
    pugi::xml_document doc;
    const pugi::xml_parse_result result = doc.load_string(body.c_str());
    if (result.status != pugi::status_ok)
    {
        Error("xml", result.description());
        return false;
    }

    auto responses = doc.first_element_by_path("d:multistatus");
    if (!responses)
    {
        Error("dav", "multistatus element not found");
        return false;
    }

    for (const auto& response : responses.children("d:response"))
    {
        auto elStatus = response.first_element_by_path("d:propstat/d:status");
        if (!elStatus)
        {
            Error("dav", "No status element");
            return false;
        }
        if (!std::string(elStatus.text().as_string()).ends_with("200 OK"))
        {
            Error("dav", elStatus.text().as_string());
            return false;
        }

        std::string href;
        std::string etag;

        auto elHref = response.child("d:href");
        if (!elHref)
        {
            Error("dav", "No href found");
            return false;
        }
        href = elHref.text().as_string();

        auto elEtag = response.first_element_by_path("d:propstat/d:prop/d:getetag");
        if (elEtag)
            etag = elEtag.text().as_string();

        auto elCalData = response.first_element_by_path("d:propstat/d:prop/cal:calendar-data");
        if (!elCalData)
        {
            Error("dav", "No calendar-data found");
            return false;
        }

        Event event(std::move(href), std::move(etag), name_, color_);
        if (!event.Parse(elCalData.text().as_string()))
        {
            Error("dav", "Invalid calendar-data");
            continue;
        }
        events.push_back(std::move(event));
    }

    return true;
}

bool Calendar::Init()
{
    if (propfind_)
        return true;
    return Propfind();
}

bool Calendar::Propfind()
{
    // https://sabre.io/dav/clients/ical/
    static constexpr std::string_view requestXML = R"xml(<?xml version="1.0" encoding="UTF-8"?>
<d:propfind xmlns:d="DAV:" xmlns:cs="http://calendarserver.org/ns/" xmlns:x3="http://apple.com/ns/ical/">
    <d:prop>
        <d:displayname />
        <cs:getctag />
        <d:sync-token />
        <x3:calendar-color />
    </d:prop>
</d:propfind>)xml";
    StringWriter writer;
    if (!Client::Propfind(std::string(requestXML), 0, writer))
        return false;

    if (!ParsePropfind(writer.contents_))
        return false;
    propfind_ = true;
    return true;
}

bool Calendar::ParsePropfind(const std::string& body)
{
    pugi::xml_document doc;
    const pugi::xml_parse_result result = doc.load_string(body.c_str());
    if (result.status != pugi::status_ok)
    {
        Error("xml", result.description());
        return false;
    }

    auto prop = doc.first_element_by_path("d:multistatus/d:response/d:propstat/d:prop");
    if (!prop)
    {
        Error("dav", "multistatus/response/propstat/prop element not found");
        return false;
    }

    std::string displayName;
    std::string ctag;
    std::string synctoken;
    std::string color;

    auto props = prop.children();
    for (const auto& prop : props)
    {
        std::string name = prop.name();
        if (name.ends_with("displayname"))
            displayName = prop.text().as_string();
        else if (name.ends_with("getctag"))
            ctag = prop.text().as_string();
        else if (name.ends_with("sync-token"))
            synctoken = prop.text().as_string();
        else if (name.ends_with("calendar-color"))
            color = prop.text().as_string();
    }

    auto stat = doc.first_element_by_path("d:multistatus/d:response/d:propstat/d:status");
    if (!stat)
    {
        Error("dav", "Status not found");
        return false;
    }
    std::string status = stat.text().as_string();
    if (!status.ends_with("200 OK"))
    {
        Error("dav", status);
        return false;
    }

    name_ = displayName;
    syncToken_ = synctoken;
    ctag_ = ctag;
    if (!color.empty())
        color_ = Color::FromString(color);

    return true;
}

}
