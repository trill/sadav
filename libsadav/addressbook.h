/**
 * Copyright 2022, Stefan Ascher
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "client.h"
#include "contact.h"
#include <vector>
#include <optional>

namespace sa::dav {

class AddressBook final : public Client
{
public:
    AddressBook(std::string url, std::string username, std::string password);

    bool Propfind();
    bool GetContacts(std::vector<Contact>& result);

    std::string GetName() const override { return name_; }
    // Every time anything changes, the ctag must also change.
    // So the data can be cached.
    const std::string& GetCtag() const { return ctag_; }
private:
    bool ParsePropfind(const std::string& body);
    bool ParseReport(const std::string& body, std::vector<Contact>& contacts);
    bool Init() override;

    std::string name_;
    std::string ctag_;
    bool propfind_{ false };
};

}
